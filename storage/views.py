from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.views import APIView

from fituu.permissions import IsOwnerPermission
from storage import s3
from storage.models import FileAccess, StorageFile
from storage.serializers import StorageFileSerializer


class RefreshUrlView(APIView):
    def post(self, request):
        url = request.data.get('url')
        if not url:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'details': 'Field `url` is required'})
        try:
            access = FileAccess.objects.get(owner=request.user, url=url)
            if access.file.public:
                return Response(status=status.HTTP_400_BAD_REQUEST, data={'details': 'File is public'})
            url = s3.get_url(f'{str(access.file.id)}.{access.file.extension}')
            if not url:
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            new_access = FileAccess.objects.create(owner=request.user, file=access.file, url=url)
            return Response(data={
                'url': new_access.url,
                'expires_in': new_access.expires_in
            })
        except FileAccess.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'details': 'Given url is incorrect'})


class StorageFileListCreateView(generics.ListCreateAPIView):
    serializer_class = StorageFileSerializer
    queryset = StorageFile.objects.all()

    def filter_queryset(self, queryset):
        queryset = queryset.filter(owner=self.request.user)
        file_type = self.request.query_params.get('type')
        return queryset.filter(type=file_type) if file_type else queryset


class StorageFileRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = StorageFileSerializer
    queryset = StorageFile.objects.all()
    permission_classes = [IsOwnerPermission]

    def perform_destroy(self, instance):
        s3.delete_object(f'{instance.file.id}.{instance.file.extension}')
        instance.delete()
