from django.db import models
from django.utils import timezone

from storage import settings


def default_expiration_time():
    return timezone.now() + timezone.timedelta(seconds=settings.ACCESS_EXPIRES_IN)


class MyFile(models.Model):
    id = models.AutoField(primary_key=True)
    url = models.CharField(max_length=200, default="")
    public = models.BooleanField(default=False)
    extension = models.CharField(max_length=10, null=True)
    size_in_mb = models.IntegerField(default=0)


class FileAccess(models.Model):
    id = models.AutoField(primary_key=True)
    file = models.ForeignKey(MyFile, on_delete=models.CASCADE)
    owner = models.ForeignKey('fituu.User', on_delete=models.CASCADE)
    url = models.CharField(max_length=500, default="")
    expires_in = models.DateTimeField(default=default_expiration_time)

    class Meta:
        ordering = ['expires_in']


class StorageFile(models.Model):
    id = models.AutoField(primary_key=True)
    file = models.ForeignKey(MyFile, on_delete=models.CASCADE)
    name = models.CharField(max_length=150)
    type = models.CharField(max_length=30)
    owner = models.ForeignKey('fituu.User', on_delete=models.CASCADE)
