from django.db.models import Sum
from rest_framework import serializers
from django.utils import timezone

from storage import s3, settings
from storage.exceptions import FileTooLargeError, OutOfAvailableSpaceError
from storage.models import MyFile, FileAccess, StorageFile


class MyFileSerializer(serializers.BaseSerializer):
    def to_internal_value(self, data):
        if isinstance(data, dict):
            if 'public' in data and ('file' in data or 'url' in data):
                return data

        if isinstance(data, str):
            return {
                "url": data,
                "public": True
            }

        try:
            data_size_in_mb = data.size >> 20
            if data_size_in_mb > settings.MAX_FILE_SIZE_IN_MB:
                raise FileTooLargeError()

            return {
                "file": data.file,
                "size": data_size_in_mb,
                "extension": data.name.split(".")[-1],
                "public": self.context.get('file_public', False)
            }
        except AttributeError:
            raise serializers.ValidationError({
                'details': '`url` or `file` is required.'
            })

    def to_representation(self, instance):
        if instance.public:
            return {
                'url': instance.url
            }
        user = self.context['request'].user
        try:
            access = FileAccess.objects.filter(owner=user, file=instance).last()
            if access.expires_in > timezone.now():
                return {
                    'url': access.url,
                    'expires_in': access.expires_in
                }
        except AttributeError:
            pass

        access = FileAccess.objects.create(owner=user, file=instance)
        new_url = s3.get_url(f'{str(access.file.id)}.{access.file.extension}')

        if new_url:
            access.url = new_url
            access.save()

        return {
            'url': access.url,
            'expires_in': access.expires_in
        }

    def create(self, validated_data):
        file = MyFile.objects.create(public=validated_data.get('public'),
                                     extension=validated_data.get('extension'),
                                     size_in_mb=validated_data.get('size', 0))

        if 'file' in validated_data:
            is_ok = s3.put_object(validated_data.pop('file'),
                                  f'{str(file.id)}.{file.extension}', file.public)
            if is_ok and file.public:
                file.url = f'{settings.PUBLIC_FILES_URL}{str(file.id)}.{file.extension}'
        else:
            file.url = validated_data.get('url')
        file.save()
        return file

    def update(self, instance, validated_data):
        instance.public = validated_data.get('public', instance.public)
        instance.extension = validated_data.get('extension')
        instance.size_in_mb = validated_data.get('size', 0)

        if 'file' in validated_data:
            is_ok = s3.put_object(validated_data.pop('file'), f'{str(instance.id)}.{instance.extension}',
                                  instance.public)
            if is_ok and instance.public:
                instance.url = f'{settings.PUBLIC_FILES_URL}{str(instance.id)}.{instance.extension}'
        else:
            instance.url = validated_data.get('url', instance.url)
        instance.save()
        return instance


class StorageFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = StorageFile
        exclude = ['owner']

    file = MyFileSerializer()

    def create(self, validated_data):
        user = self.context['request'].user
        size = StorageFile.objects.filter(owner=user).aggregate(Sum('file__size_in_mb'))['file__size_in_mb__sum']
        if size and size > settings.STORAGE_SIZE_PER_USER_IN_MB:
            raise OutOfAvailableSpaceError()

        file = validated_data.pop('file')
        serializer = MyFileSerializer(data=file)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return StorageFile.objects.create(**validated_data, file=serializer.instance, owner=user)

    def update(self, instance, validated_data):
        if 'file' in validated_data:
            serializer = MyFileSerializer(instance.file, validated_data.pop('file'))
            serializer.is_valid(raise_exception=True)
            serializer.save()

        instance.name = validated_data.get('name', instance.name)
        instance.type = validated_data.get('type', instance.type)
        instance.save()

        return instance
