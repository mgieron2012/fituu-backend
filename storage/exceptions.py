from rest_framework.exceptions import APIException

from storage import settings


class OutOfAvailableSpaceError(APIException):
    status_code = 450
    default_detail = f'User has reached limit of available space: {settings.STORAGE_SIZE_PER_USER_IN_MB} MB'


class FileTooLargeError(APIException):
    status_code = 451
    default_detail = f'File is too large, max size: {settings.MAX_FILE_SIZE_IN_MB} MB'
