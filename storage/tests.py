from django.http import HttpRequest
from rest_framework.request import Request

import storage.settings
from fituu.models import User
from fituu.tests import MyAPITestCase
from storage import s3
from storage.models import StorageFile, MyFile
from storage.serializers import MyFileSerializer
from storage.settings import PUBLIC_FILES_URL
import requests

from storage.views import RefreshUrlView, StorageFileListCreateView, StorageFileRetrieveUpdateDestroyView


class StorageTest(MyAPITestCase):
    def test_put(self):
        file_content = open(".gitignore", mode="br").read()

        response = s3.put_object(file_content, "test", True)
        self.assertEqual(response['ResponseMetadata']['HTTPStatusCode'], 200)

    def test_get_url(self):
        url = s3.get_url("test")
        self.assertIsNotNone(url)

    def test_delete(self):
        file_content = open(".gitignore", mode="br").read()

        response = s3.put_object(file_content, "some_key", True)
        self.assertEqual(response['ResponseMetadata']['HTTPStatusCode'], 200)
        self.assertEqual(200, requests.get(url=PUBLIC_FILES_URL + "some_key").status_code)

        response = s3.delete_object("some_key")
        self.assertEqual(response['ResponseMetadata']['HTTPStatusCode'], 204)
        self.assertEqual(403, requests.get(url=PUBLIC_FILES_URL + "some_key").status_code)

    def test_create(self):
        file = open("profilowe.png", "br").read()
        instance = MyFileSerializer(data={'public': False, 'file': file})
        req = Request(HttpRequest())
        req.user = self.user
        instance.context['request'] = req
        instance.is_valid(True)
        instance.save()
        self.assertEqual(200, requests.get(url=instance.data.get('url')).status_code)

    def test_update(self):
        file = open("profilowe.png", "br").read()
        serializer = MyFileSerializer(data={'public': True, 'file': file})
        req = Request(HttpRequest())
        req.user = self.user
        serializer.context['request'] = req
        serializer.is_valid(True)
        serializer.save()
        instance = serializer.instance
        self.assertEqual(f'{storage.settings.PUBLIC_FILES_URL}{str(instance.id)}.{instance.extension}',
                         instance.url)
        self.assertEqual(200, requests.get(url=instance.url).status_code)

        file = open("profilowe2.png", "br").read()
        serializer = MyFileSerializer(instance, data={'public': False, 'file': file})
        serializer.context['request'] = req

        serializer.is_valid(True)
        serializer.save()
        instance = serializer.instance

        self.assertFalse(instance.public)
        self.assertNotEqual(storage.settings.PUBLIC_FILES_URL + str(instance.id), serializer.data.get('url'))
        self.assertEqual(403, requests.get(url=instance.url).status_code)
        self.assertEqual(200, requests.get(url=serializer.data.get('url')).status_code)

    def test_create_with_url(self):
        serializer = MyFileSerializer(data='some_url')
        req = Request(HttpRequest())
        req.user = self.user

        serializer.context['request'] = req
        serializer.is_valid(True)
        serializer.save()
        instance = serializer.instance
        self.assertEqual(instance.url, "some_url")
        self.assertTrue(instance.public)

    def test_refresh_view(self):
        file = open("profilowe.png", "br").read()
        serializer = MyFileSerializer(data={'public': False, 'file': file, 'extension': 'png'})
        req = Request(HttpRequest())
        req.user = self.user
        serializer.context['request'] = req
        serializer.is_valid(True)
        serializer.save()
        url = serializer.data.get('url')
        response = self.auth_request('', RefreshUrlView, "POST", {'url': url})
        self.assertEqual(response.status_code, 200)
        new_url = response.data.get('url')
        self.assertEqual(200, requests.get(url=new_url).status_code)

    def test_refresh_view_no_perm(self):
        file = open("profilowe.png", "br").read()
        serializer = MyFileSerializer(data={'public': False, 'file': file})
        req = Request(HttpRequest())
        req.user = User.objects.first()
        serializer.context['request'] = req
        serializer.is_valid(True)
        serializer.save()
        url = serializer.data.get('url')
        response = self.auth_request('', RefreshUrlView, "POST", {'url': url})
        self.assertEqual(response.status_code, 400)

    def test_create_storage_file(self):
        data_to_send = {
            'name': 'profilowka',
            'type': "profile",
            'file': 'some_url'
        }

        response = self.auth_request('url', StorageFileListCreateView, "POST", data_to_send)
        file = StorageFile.objects.get(id=response.data['id'])
        self.assertEqual(file.owner, self.user)
        self.assertEqual(file.name, data_to_send['name'])
        self.assertEqual(file.type, data_to_send['type'])

    def test_update_storage_file(self):
        data_to_send = {
            'name': 'profilowka 2',
            'type': "profile",
            'file': 'new_url'
        }

        response = self.auth_request('url', StorageFileRetrieveUpdateDestroyView, "PATCH", data_to_send,
                                     pk=StorageFile.objects.first().id)
        file = StorageFile.objects.get(id=response.data['id'])
        self.assertEqual(file.owner, self.user)
        self.assertEqual(file.name, data_to_send['name'])
        self.assertEqual(file.type, data_to_send['type'])
        self.assertEqual(file.file.url, data_to_send['file'])

    def test_retrieve_storage_file(self):
        response = self.auth_request('url', StorageFileRetrieveUpdateDestroyView, "GET",
                                     pk=StorageFile.objects.first().id)
        file = StorageFile.objects.get(id=response.data['id'])
        self.assertEqual(file.name, response.data['name'])
        self.assertEqual(file.type, response.data['type'])
        self.assertEqual(file.file.url, response.data['file']['url'])

    def test_delete_storage_file(self):
        storage_file = StorageFile.objects.filter(owner=self.user).first()
        self.auth_request('url', StorageFileRetrieveUpdateDestroyView, "DELETE", pk=storage_file.id)
        with self.assertRaises(StorageFile.DoesNotExist):
            storage_file.refresh_from_db()

    def test_delete_storage_file_no_perm(self):
        storage_file = StorageFile.objects.filter(owner=self.user).first()
        storage_file.owner = User.objects.first()
        storage_file.save()
        response = self.auth_request('url', StorageFileRetrieveUpdateDestroyView, "DELETE", pk=storage_file.id)
        storage_file.refresh_from_db()
        self.assertEqual(response.status_code, 403)

    def test_list_storage_file(self):
        file = MyFile.objects.create(url='url', public=True)
        StorageFile.objects.create(owner=self.user, name="f1", type="t1", file=file)
        StorageFile.objects.create(owner=self.user, name="f2", type="t1", file=file)
        StorageFile.objects.create(owner=self.user, name="f3", type="t2", file=file)
        StorageFile.objects.create(owner=User.objects.first(), name="f3", type="t2", file=file)
        response = self.auth_request('url', StorageFileListCreateView, "GET", data={'type': 't1'})
        data = response.data
        self.assertEqual(data[0]['name'], "f1")
        self.assertEqual(data[0]['type'], "t1")
        self.assertEqual(data[0]['file']['url'], "url")
        self.assertEqual(data[1]['name'], "f2")
        self.assertEqual(len(data), 2)
