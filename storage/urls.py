from django.urls import path
from storage.views import RefreshUrlView, StorageFileListCreateView, StorageFileRetrieveUpdateDestroyView

urlpatterns = [
    path('refreshURL/', RefreshUrlView.as_view()),
    path('', StorageFileListCreateView.as_view()),
    path('<int:pk>/', StorageFileRetrieveUpdateDestroyView.as_view()),
]
