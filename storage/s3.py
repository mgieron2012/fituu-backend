import boto3
from botocore.exceptions import ClientError
import logging

from storage import settings

client = boto3.client(
    's3',
    'eu-central-1',
    aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
    aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
)


def put_object(file, key, public=False):
    try:
        return client.put_object(ACL='private', Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                                 Body=file, Key=key, Tagging=f"public={'yes' if public else 'no'}")
    except ClientError as e:
        logging.error(e)


def delete_object(key):
    try:
        return client.delete_object(
            Bucket=settings.AWS_STORAGE_BUCKET_NAME,
            Key=key)
    except ClientError as e:
        logging.error(e)


def get_url(key):
    try:
        return client.generate_presigned_url("get_object", ExpiresIn=settings.ACCESS_EXPIRES_IN,
                                             Params={'Key': key, 'Bucket': settings.AWS_STORAGE_BUCKET_NAME})
    except ClientError as e:
        logging.error(e)
