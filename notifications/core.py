from notifications.models import Notification, NotificationStatus
from django.apps import apps


def notify(title, content, users):
    notification = Notification.objects.create(title=title, content=content)
    for user in users:
        NotificationStatus.objects.create(notification=notification, owner=user)


def notify_trainers(title, content):
    trainers = apps.get_model('fituu.User').objects.exclude(type=3)
    notify(title, content, trainers)


def notify_clients(title, content):
    clients = apps.get_model('fituu.User').objects.filter(type=3)
    notify(title, content, clients)


def notify_all(title, content):
    clients = apps.get_model('fituu.User').objects.all()
    notify(title, content, clients)
