from django.db import models


class Notification(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
    content = models.TextField()
    created = models.DateTimeField(auto_now=True)


class NotificationStatus(models.Model):
    id = models.AutoField(primary_key=True)
    owner = models.ForeignKey('fituu.User', on_delete=models.CASCADE)
    is_read = models.BooleanField(default=False)
    notification = models.ForeignKey(Notification, on_delete=models.CASCADE)
