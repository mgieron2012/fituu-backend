from rest_framework import serializers

from notifications.models import Notification, NotificationStatus


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        exclude = ['id']


class NotificationStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotificationStatus
        exclude = ['owner']

    notification = NotificationSerializer(read_only=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        notification = representation.pop('notification')
        return representation | notification
