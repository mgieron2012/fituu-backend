from django.urls import path

from notifications.views import NotificationRetrieveDestroyView, NotificationListView

urlpatterns = [
    path('', NotificationListView.as_view()),
    path('<int:pk>/', NotificationRetrieveDestroyView.as_view())
]
