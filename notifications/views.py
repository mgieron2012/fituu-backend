from rest_framework import generics

from fituu.permissions import IsOwnerPermission
from notifications.models import NotificationStatus
from notifications.serializers import NotificationStatusSerializer


class NotificationListView(generics.ListAPIView):
    serializer_class = NotificationStatusSerializer
    queryset = NotificationStatus.objects.all()

    def filter_queryset(self, queryset):
        if timestamp := self.request.query_params.get('timestamp'):
            queryset = queryset.filter(notification__created__gt=timestamp)
        return queryset.filter(owner=self.request.user)


class NotificationRetrieveDestroyView(generics.RetrieveDestroyAPIView):
    serializer_class = NotificationStatusSerializer
    permission_classes = [IsOwnerPermission]
    queryset = NotificationStatus.objects.all()

    def get_object(self):
        obj = super().get_object()
        obj.is_read = True
        obj.save()
        return obj
