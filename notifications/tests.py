from fituu.models import User
from fituu.tests import MyAPITestCase
from notifications.core import notify, notify_all, notify_clients, notify_trainers
from notifications.models import NotificationStatus, Notification
from notifications.views import NotificationListView, NotificationRetrieveDestroyView


class NotificationTest(MyAPITestCase):
    def test_notify(self):
        title = "Title"
        content = "Content"

        notify(title, content, [self.user])

        status = NotificationStatus.objects.get(owner=self.user)
        self.assertEqual(status.notification.title, title)
        self.assertEqual(status.notification.content, content)

    def test_notify_all(self):
        title = "Title"
        content = "Content"

        notification_number = Notification.objects.count()
        notify_all(title, content)
        self.assertEqual(Notification.objects.count(), notification_number+1)
        status = NotificationStatus.objects.get(owner=self.user)
        self.assertEqual(status.notification.title, title)
        self.assertEqual(status.notification.content, content)
        self.assertFalse(status.is_read)

    def test_notify_clients(self):
        title = "Title"
        content = "Content"

        self.user.type = 3
        self.user.save()
        notify_clients(title, content)
        status = NotificationStatus.objects.get(owner=self.user)
        self.assertEqual(status.notification.title, title)
        self.assertEqual(status.notification.content, content)
        self.assertFalse(status.is_read)

    def test_notify_clients_not_send_to_trainers(self):
        title = "Title"
        content = "Content"

        self.user.type = 1
        self.user.save()
        notify_clients(title, content)
        with self.assertRaises(NotificationStatus.DoesNotExist):
            NotificationStatus.objects.get(owner=self.user)

    def test_notify_trainers(self):
        title = "Title"
        content = "Content"

        self.user.type = 1
        self.user.save()
        notify_trainers(title, content)
        status = NotificationStatus.objects.get(owner=self.user)
        self.assertEqual(status.notification.title, title)
        self.assertEqual(status.notification.content, content)
        self.assertFalse(status.is_read)

    def test_notify_trainers_not_send_to_clients(self):
        title = "Title"
        content = "Content"

        self.user.type = 3
        self.user.save()
        notify_trainers(title, content)
        with self.assertRaises(NotificationStatus.DoesNotExist):
            NotificationStatus.objects.get(owner=self.user)

    def test_notification_list_view(self):
        notify("Title", "Content", [self.user])
        notify("Title 2", "Content 2", [self.user])

        response = self.auth_request('not/', NotificationListView, "GET")
        self.assertEqual(len(response.data), NotificationStatus.objects.filter(owner=self.user).count())
        self.assertEqual(response.data[0]['title'], "Title")
        self.assertEqual(response.data[0]['content'], "Content")
        self.assertEqual(response.data[0]['is_read'], False)

    def test_notification_list_view_with_last_param(self):
        notify("Title.", "Content", [self.user])
        notify("Title 2", "Content 2", [self.user])
        notification = Notification.objects.get(title="Title.", content="Content")
        status = NotificationStatus.objects.get(owner=self.user, notification=notification)

        response = self.auth_request('not/', NotificationListView, "GET", {'timestamp': status.notification.created})
        self.assertEqual(len(response.data),
                         NotificationStatus.objects.
                         filter(owner=self.user, notification__created__gt=status.notification.created).count())
        self.assertNotEqual(response.data[0]['title'], "Title.")
        self.assertEqual(response.data[0]['content'], "Content 2")
        self.assertEqual(response.data[0]['is_read'], False)

    def test_notification_retrieve_view(self):
        notify("Title.", "Content", [self.user])
        notification = Notification.objects.get(title="Title.", content="Content")
        status = NotificationStatus.objects.get(owner=self.user, notification=notification)
        response = self.auth_request('not/', NotificationRetrieveDestroyView, "GET", pk=status.id)
        self.assertTrue(response.data['is_read'])
        self.assertEqual(response.data['title'], "Title.")

    def test_notification_delete_view_no_perm(self):
        notify("Title.", "Content", [User.objects.first()])
        notification = Notification.objects.get(title="Title.", content="Content")
        status = NotificationStatus.objects.get(owner=User.objects.first(), notification=notification)
        response = self.auth_request('not/', NotificationRetrieveDestroyView, "DELETE", pk=status.id)
        self.assertEqual(response.status_code, 403)

    def test_notification_delete(self):
        notify("Title.", "Content", [self.user])
        notification = Notification.objects.get(title="Title.", content="Content")
        status = NotificationStatus.objects.get(owner=self.user, notification=notification)
        response = self.auth_request('not/', NotificationRetrieveDestroyView, "DELETE", pk=status.id)
        self.assertEqual(response.status_code, 204)
        with self.assertRaises(NotificationStatus.DoesNotExist):
            status.refresh_from_db()
