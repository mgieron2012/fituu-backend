from datetime import datetime

from rest_framework import serializers
from fituu.models import User, Tag, Offer, Exercise, BodyPart, Set, ExercisesSet, Training, \
    TrainingSets, TrainingExercises, ExerciseType, ExercisePattern
from fituu.utils import in_data_or_raise_validation_error
from storage.serializers import MyFileSerializer, StorageFileSerializer


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ['id', 'title']


class UserSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'first_name', 'last_name', 'profile_photo']

    profile_photo = MyFileSerializer()


class UserPrivateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)
    profile_photo = MyFileSerializer()
    trainers = serializers.SerializerMethodField()

    class Meta:
        model = User
        exclude = ['username', 'is_admin', 'is_active', 'is_staff']

    def get_trainers(self, instance):
        return UserSimpleSerializer([relation.trainer for relation in instance.trainers.all()], many=True).data


class UserPublicSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)
    profile_photo = MyFileSerializer()

    class Meta:
        model = User
        fields = ['id', 'email', 'first_name', 'last_name', 'city', 'verified',
                  'tags', 'profile_photo', 'description', 'certificates', 'type']


class UserForTrainerSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'first_name', 'last_name', 'profile_photo', 'weight', 'height', 'age']

    age = serializers.SerializerMethodField()
    profile_photo = MyFileSerializer()

    def get_age(self, instance):
        if instance.birth_date:
            now = datetime.now()
            if now.month < instance.birth_date.month:
                return now.year - instance.birth_date.year - 1
            if now.month == instance.birth_date.month and now.day < instance.birth_date.day:
                return now.year - instance.birth_date.year - 1
            return now.year - instance.birth_date.year


class MutableUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'city', 'tags', 'profile_photo', 'description', 'certificates', 'type',
                  'postal_code', 'address', 'birth_date', 'weight', 'height']

    postal_code = serializers.RegexField(regex=r'\d{2}-\d{3}')
    profile_photo = MyFileSerializer()

    def update(self, instance, validated_data):
        if 'profile_photo' in validated_data:
            profile_photo_data = validated_data.pop('profile_photo')
            if instance.profile_photo:
                serializer = MyFileSerializer(instance.profile_photo, data=profile_photo_data)
            else:
                serializer = MyFileSerializer(data=profile_photo_data)

            if serializer.is_valid(raise_exception=True):
                instance.profile_photo = serializer.save()
                instance.save()
        return super().update(instance, validated_data)


class OfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = Offer
        exclude = ['owner']

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'title': instance.title,
            'description': instance.description,
            'tags': TagSerializer(instance.tags, many=True).data,
            'owner': UserSimpleSerializer(instance.owner).data,
            'price_in_gr': instance.price_in_gr,
            'price_description': instance.price_description,
            'active': instance.active
        }


class BodyPartSerializer(serializers.ModelSerializer):
    class Meta:
        model = BodyPart
        fields = "__all__"


class ExerciseTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExerciseType
        fields = "__all__"


class ExercisePatternSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExercisePattern
        fields = "__all__"


class ExerciseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exercise
        exclude = ['owner']

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'description': instance.description,
            'body_parts': BodyPartSerializer(instance.body_parts, many=True).data,
            'patterns': ExercisePatternSerializer(instance.patterns, many=True).data,
            'types': ExerciseTypeSerializer(instance.types, many=True).data,
            'media': StorageFileSerializer(instance.media).data if instance.media else None
        }

    def validate_media(self, value):
        if value is not None and value.owner != self.context['request'].user:
            raise serializers.ValidationError({'media': 'Permission denied.'})
        return value


class ExercisesSetSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExercisesSet
        exclude = ['set']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation.pop('exercise')
        return representation | ExerciseSerializer(instance.exercise).data


class SetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Set
        exclude = ['owner']

    exercises = ExercisesSetSerializer(many=True)

    def create(self, validated_data):
        exercises = validated_data.pop('exercises')
        created_set = Set.objects.create(**validated_data)

        for exercise in exercises:
            ExercisesSet.objects.create(set=created_set, **exercise)
        return created_set

    def update(self, instance, validated_data):
        if 'exercises' in validated_data:
            ExercisesSet.objects.filter(set=instance).delete()
            exercises = validated_data.pop('exercises')
            for exercise in exercises:
                ExercisesSet.objects.create(set=instance, **exercise)
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)
        instance.save()
        return instance

    def validate_exercises(self, value):
        user = self.context['request'].user
        for exercise in value:
            if exercise['exercise'].owner != user:
                raise serializers.ValidationError({'exercise': 'Permission denied'})
        return value


class TrainingSetSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainingSets
        exclude = ['training']

    set = SetSerializer()

    def to_internal_value(self, data):
        in_data_or_raise_validation_error(data, ['id', 'reps', 'order'])

        if not Set.objects.filter(id=data['id']).exists():
            raise serializers.ValidationError({
                'id': 'Given id does not point to set instance'
            })

        return {
            'set_id': data['id'],
            'reps': data['reps'],
            'order': data['order'],
            'note': data.get('note', '')
        }

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        set = representation.pop('set')

        return representation | set


class TrainingExercisesSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainingExercises
        exclude = ['training']

    exercise = ExerciseSerializer()

    def to_internal_value(self, data):
        in_data_or_raise_validation_error(data, ['id', 'reps', 'order'])

        if not Exercise.objects.filter(id=data['id']).exists():
            raise serializers.ValidationError({
                'id': 'Given id does not point to exercise instance'
            })

        return {
            'exercise_id': data['id'],
            'reps': data['reps'],
            'order': data['order'],
            'note': data.get('note', '')
        }

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        exercise = representation.pop('exercise')

        return representation | exercise


class TrainingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Training
        exclude = ['owner']

    exercises = TrainingExercisesSerializer(many=True)
    sets = TrainingSetSerializer(many=True)

    def create(self, validated_data):
        exercises = validated_data.pop('exercises')
        sets = validated_data.pop('sets')
        training = Training.objects.create(**validated_data)

        for set_data in sets:
            TrainingSets.objects.create(training=training, **set_data)

        for exercise in exercises:
            TrainingExercises.objects.create(training=training, **exercise)

        return training

    def update(self, instance, validated_data):
        if 'exercises' in validated_data:
            exercises = validated_data.pop('exercises')
            TrainingExercises.objects.filter(training=instance).delete()

            for exercise in exercises:
                TrainingExercises.objects.create(training=instance, **exercise)

        if 'sets' in validated_data:
            sets = validated_data.pop('sets')
            TrainingSets.objects.filter(training=instance).delete()

            for set_data in sets:
                TrainingSets.objects.create(training=instance, **set_data)

        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)
        instance.save()
        return instance

    def validate_exercises(self, value):
        user = self.context['request'].user
        for exercise in value:
            if not Exercise.objects.filter(id=exercise['exercise_id'], owner=user).exists():
                raise serializers.ValidationError({'exercise': 'Permission denied'})
        return value

    def validate_sets(self, value):
        user = self.context['request'].user
        for set in value:
            if not Set.objects.filter(id=set['set_id'], owner=user).exists():
                raise serializers.ValidationError({'set': 'Permission denied'})
        return value
