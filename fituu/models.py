from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.db import models

from fituu.utils import send_new_client_notification
from storage.models import MyFile


class Tag(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=50)

    class Meta:
        ordering = ['title']


class UserManager(BaseUserManager):
    def create_user(self, username, email, first_name, last_name):
        user = self.model(
            username=username,
            email=email,
            first_name=first_name,
            last_name=last_name
        )

        user.save()

        invitations = TrainerInvitation.objects.filter(client_email=email)
        for invitation in invitations:
            trainer_client = TrainerClients.objects.create(trainer=invitation.trainer, client=user)
            send_new_client_notification(trainer_client)
            invitation.delete()

        return user

    def create_superuser(self, username, email, name):
        user = self.model(
            username=username,
            email=email,
            name=name,
            is_admin=True
        )
        user.save()
        return user


class User(AbstractBaseUser):
    TYPES = [
        (1, 'Trener personalny'),
        (2, 'Dietetyk'),
        (3, 'Klient')
    ]

    id = models.AutoField(primary_key=True)
    type = models.SmallIntegerField(default=3, choices=TYPES)
    username = models.CharField(max_length=100, unique=True)
    email = models.CharField(max_length=100, unique=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    city = models.CharField(max_length=80, default="")
    tags = models.ManyToManyField(Tag, blank=True)
    profile_photo = models.ForeignKey(MyFile, on_delete=models.CASCADE, null=True)
    description = models.TextField(default="")
    certificates = models.TextField(default="")
    verified = models.BooleanField(default=False)

    # information for payments
    postal_code = models.CharField(default="", max_length=6)
    address = models.CharField(default="", max_length=100)

    # information for trainer
    birth_date = models.DateField(null=True)
    weight = models.IntegerField(null=True)
    height = models.IntegerField(null=True)

    # additional
    @property
    def is_female(self):
        return self.first_name != '' and self.first_name[-1] == 'a'

    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    objects = UserManager()

    def __str__(self):
        return self.email

    class Meta:
        ordering = ['last_name', 'first_name']


class Offer(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.TextField()
    description = models.TextField()
    tags = models.ManyToManyField(Tag, blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    price_in_gr = models.IntegerField(null=True)
    price_description = models.CharField(max_length=100, default="")
    active = models.BooleanField(default=True)


class BodyPart(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ['name']


class ExerciseType(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)

    class Meta:
        ordering = ['title']


class ExercisePattern(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)

    class Meta:
        ordering = ['title']


class Exercise(models.Model):
    id = models.AutoField(primary_key=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    description = models.TextField()
    body_parts = models.ManyToManyField(BodyPart, blank=True)
    patterns = models.ManyToManyField(ExercisePattern, blank=True)
    types = models.ManyToManyField(ExerciseType, blank=True)
    media = models.ForeignKey('storage.StorageFile', on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ['name']


class Set(models.Model):
    id = models.AutoField(primary_key=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    description = models.TextField()

    class Meta:
        ordering = ['name']


class ExercisesSet(models.Model):
    id = models.AutoField(primary_key=True)
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    set = models.ForeignKey(Set, on_delete=models.CASCADE, related_name="exercises")
    reps = models.CharField(max_length=50)
    order = models.IntegerField()

    class Meta:
        ordering = ['order']


class Training(models.Model):
    id = models.AutoField(primary_key=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    description = models.TextField()

    class Meta:
        ordering = ['name']


class TrainingSets(models.Model):
    set = models.ForeignKey(Set, on_delete=models.CASCADE)
    training = models.ForeignKey(Training, on_delete=models.CASCADE, related_name='sets')
    reps = models.CharField(max_length=50)
    order = models.IntegerField()
    note = models.TextField(default="")

    class Meta:
        ordering = ['order']


class TrainingExercises(models.Model):
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    training = models.ForeignKey(Training, on_delete=models.CASCADE, related_name='exercises')
    order = models.IntegerField()
    note = models.TextField(default="")
    reps = models.CharField(max_length=50)

    class Meta:
        ordering = ['order']


class TrainerClients(models.Model):
    id = models.AutoField(primary_key=True)
    trainer = models.ForeignKey(User, on_delete=models.CASCADE, related_name="clients")
    client = models.ForeignKey(User, on_delete=models.CASCADE, related_name="trainers")


class TrainerInvitation(models.Model):
    id = models.AutoField(primary_key=True)
    trainer = models.ForeignKey(User, on_delete=models.CASCADE)
    client_email = models.CharField(max_length=100)
