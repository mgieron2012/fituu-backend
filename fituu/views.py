from django.db.models import Q, F
from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from fituu.models import Tag, User, Offer, Exercise, BodyPart, Set, Training, TrainerClients, ExercisePattern, \
    ExerciseType, TrainerInvitation
from fituu.pagination import OfferListPagination
from fituu.permissions import IsOwnerPermissionAllowGet, IsOwnerPermission, IsTrainerPermission, \
    IsOwnerPermissionAllowGetForClients
from fituu.serializers import MutableUserSerializer, TagSerializer, UserPublicSerializer, ExerciseTypeSerializer,\
    ExercisePatternSerializer, UserForTrainerSerializer, ExerciseSerializer, BodyPartSerializer, SetSerializer,\
    TrainingSerializer, UserPrivateSerializer, UserSimpleSerializer, OfferSerializer
from fituu.utils import send_new_client_notification, send_delete_client_notification, send_invitation_email


class UserUpdateView(generics.UpdateAPIView):
    serializer_class = MutableUserSerializer

    def get_object(self):
        return self.request.user

    def put(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_serializer_context(self):
        return super().get_serializer_context() | {'file_public': True}


class UserSelfInfoView(generics.RetrieveAPIView):
    serializer_class = UserPrivateSerializer

    def get_object(self):
        return self.request.user


class UserInfoView(generics.RetrieveAPIView):
    serializer_class = UserPublicSerializer
    permission_classes = [AllowAny]
    queryset = User.objects.all()


class TagListView(generics.ListAPIView):
    serializer_class = TagSerializer
    permission_classes = [AllowAny]
    queryset = Tag.objects.all()


class UserTypesView(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        return Response([{'id': user_type[0], 'name': user_type[1]} for user_type in User.TYPES])


class OfferRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsOwnerPermissionAllowGet]
    serializer_class = OfferSerializer
    queryset = Offer.objects.all()


class OfferListCreateView(generics.ListCreateAPIView):
    serializer_class = OfferSerializer
    queryset = Offer.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def filter_queryset(self, queryset):
        return queryset.filter(owner=self.request.user)


class OfferListFilter(generics.ListAPIView):
    serializer_class = OfferSerializer
    permission_classes = [AllowAny]
    pagination_class = OfferListPagination

    def get_queryset(self):
        queryset = Offer.objects.filter(active=True)
        owner = self.request.query_params.get('owner')
        if owner is not None:
            queryset = queryset.filter(owner__id=owner)
        text = self.request.query_params.get('text')
        if text is not None:
            queryset = queryset.filter(Q(title__icontains=text) | Q(description__icontains=text))
        min_price = self.request.query_params.get('min_price')
        if min_price is not None:
            queryset = queryset.filter(price_in_gr__gte=min_price)
        max_price = self.request.query_params.get('max_price')
        if max_price is not None:
            queryset = queryset.filter(price_in_gr__lte=max_price)
        price_description = self.request.query_params.get('price_description')
        if price_description is not None:
            queryset = queryset.filter(price_description=price_description)
        tags = self.request.query_params.getlist('tags')
        if tags is not None:
            for tag in tags:
                queryset = queryset.filter(tags__in=[tag])
        order = self.request.query_params.get('order')
        if order is None:
            return queryset.order_by('id')
        if order[0] == '-':
            return queryset.order_by(F(order[1:]).desc(nulls_last=True))
        return queryset.order_by(F(order).asc(nulls_last=True))


class ExerciseListCreateView(generics.ListCreateAPIView):
    serializer_class = ExerciseSerializer

    def get_queryset(self):
        return Exercise.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class ExerciseRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ExerciseSerializer
    queryset = Exercise.objects.all()
    permission_classes = [IsOwnerPermission]


class BodyPartListView(generics.ListAPIView):
    serializer_class = BodyPartSerializer
    queryset = BodyPart.objects.all()
    permission_classes = [AllowAny]


class ExercisePatternListView(generics.ListAPIView):
    serializer_class = ExercisePatternSerializer
    queryset = ExercisePattern.objects.all()
    permission_classes = [AllowAny]


class ExerciseTypeListView(generics.ListAPIView):
    serializer_class = ExerciseTypeSerializer
    queryset = ExerciseType.objects.all()
    permission_classes = [AllowAny]


class SetListCreateView(generics.ListCreateAPIView):
    serializer_class = SetSerializer

    def get_queryset(self):
        return Set.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class SetRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = SetSerializer
    queryset = Set.objects.all()
    permission_classes = [IsOwnerPermission]


class TrainingListCreateView(generics.ListCreateAPIView):
    serializer_class = TrainingSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        return Training.objects.filter(owner=self.request.user)


class TrainingRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TrainingSerializer
    queryset = Training.objects.all()
    permission_classes = [IsOwnerPermissionAllowGetForClients]


class ClientListCreateDestroyView(APIView):
    permission_classes = [IsTrainerPermission]

    def get(self, request):
        return Response(UserSimpleSerializer([client.client for client in request.user.clients.all()], many=True).data)

    def post(self, request):
        email = request.data.get('email')

        if not email:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'field `email` is required'})
        try:
            client = User.objects.get(email=email)
            trainer_client, _ = TrainerClients.objects.get_or_create(trainer=request.user, client=client)
            send_new_client_notification(trainer_client)
            return Response(status=status.HTTP_201_CREATED)
        except User.DoesNotExist:
            send_invitation_email(email, request.user)
            TrainerInvitation.objects.get_or_create(trainer=request.user, client_email=email)
            return Response(status=status.HTTP_200_OK)

    def delete(self, request):
        client = request.data.get('client')

        if not client:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'field `client` is required'})
        try:
            trainer_client = TrainerClients.objects.get(trainer=request.user, client=client)
            send_delete_client_notification(trainer_client)
            trainer_client.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except TrainerClients.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'User with given id is not in clients'})


class ClientRetrieveView(generics.RetrieveAPIView):
    serializer_class = UserForTrainerSerializer
    permission_classes = [IsTrainerPermission]
    queryset = User.objects.all()
