from django.urls import path
from fituu.views import UserSelfInfoView, UserUpdateView, \
    TagListView, UserInfoView, UserTypesView, OfferRetrieveUpdateDestroyView, OfferListFilter, ExerciseListCreateView, \
    ExerciseRetrieveUpdateDestroyView, BodyPartListView, OfferListCreateView, SetListCreateView, \
    SetRetrieveUpdateDestroyView, TrainingListCreateView, TrainingRetrieveUpdateDestroyView, \
    ClientListCreateDestroyView, ClientRetrieveView, ExercisePatternListView, ExerciseTypeListView

urlpatterns = [
    path('user/', UserSelfInfoView.as_view()),
    path('user/details/<int:pk>/', UserInfoView.as_view()),
    path('user/update/', UserUpdateView.as_view()),
    path('user/types/', UserTypesView.as_view()),
    path('tags/', TagListView.as_view()),
    path('offer/', OfferListCreateView.as_view()),
    path('offer/<int:pk>/', OfferRetrieveUpdateDestroyView.as_view()),
    path('offer/filter/', OfferListFilter.as_view()),
    path('exercise/bodyparts/', BodyPartListView.as_view()),
    path('exercise/types/', ExerciseTypeListView.as_view()),
    path('exercise/patterns/', ExercisePatternListView.as_view()),
    path('exercise/', ExerciseListCreateView.as_view()),
    path('exercise/<int:pk>/', ExerciseRetrieveUpdateDestroyView.as_view()),
    path('set/', SetListCreateView.as_view()),
    path('set/<int:pk>/', SetRetrieveUpdateDestroyView.as_view()),
    path('training/', TrainingListCreateView.as_view()),
    path('training/<int:pk>/', TrainingRetrieveUpdateDestroyView.as_view()),
    path('clients/', ClientListCreateDestroyView.as_view()),
    path('clients/<int:pk>/', ClientRetrieveView.as_view()),
]
