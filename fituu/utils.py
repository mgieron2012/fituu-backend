from djangorestframework_camel_case.settings import api_settings
from djangorestframework_camel_case.util import underscoreize
from rest_framework import serializers
from notifications.core import notify


class CamelCaseMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.GET = underscoreize(
            request.GET,
            **api_settings.JSON_UNDERSCOREIZE
        )

        response = self.get_response(request)
        return response


def in_data_or_raise_validation_error(data, values):
    for value in values:
        if value not in data:
            raise serializers.ValidationError({
                value: 'This field is required.'
            })


def send_new_client_notification(trainer_client):
    client = trainer_client.client
    trainer = trainer_client.trainer
    if trainer.is_female:
        notify("Masz nową trenerkę!", f"{trainer.first_name} {trainer.last_name} dodała Cię do swoich podopiecznych.",
               [client])
    else:
        notify("Masz nowego trenera!", f"{trainer.first_name} {trainer.last_name} dodał Cię do swoich podopiecznych.",
               [client])

    if client.is_female:
        notify("Masz nową klientkę!", f"{client.first_name} {client.last_name} została twoją podopieczną.", [trainer])
    else:
        notify("Masz nowego klienta!", f"{client.first_name} {client.last_name} został twoim podopiecznym.", [trainer])


def send_delete_client_notification(trainer_client):
    client = trainer_client.client
    trainer = trainer_client.trainer
    notify("Tracisz trenera",
           f"{trainer.first_name} {trainer.last_name} {'usunęła' if trainer.is_female else 'usunął'}"
           f" Cię z listy swoich podopiecznych.",
           [client])


def send_invitation_email(to_email, sender):
    pass
