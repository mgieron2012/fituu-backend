from rest_framework import permissions


class IsOwnerPermissionAllowGet(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.owner == request.user


class IsOwnerPermissionAllowGetForClients(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS and\
                request.user in [client.client for client in obj.owner.clients.all()]:
            return True

        return obj.owner == request.user


class IsOwnerPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user


class IsTrainerPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user is not None and request.user.type != 3

    def has_object_permission(self, request, view, obj):
        return obj.id in [client.client.id for client in request.user.clients.all()]
