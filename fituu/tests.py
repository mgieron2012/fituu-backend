import datetime

from rest_framework.test import APITestCase, APIRequestFactory, force_authenticate
from rest_framework import status
from fituu.models import User, Tag, Offer, Exercise, BodyPart, Set, ExercisesSet, Training, \
    TrainingSets, TrainingExercises, TrainerClients, ExerciseType, ExercisePattern, TrainerInvitation
from fituu.views import UserSelfInfoView, UserUpdateView, ExerciseRetrieveUpdateDestroyView, ExerciseListCreateView, \
    OfferListCreateView, OfferRetrieveUpdateDestroyView, SetListCreateView, SetRetrieveUpdateDestroyView, \
    TrainingListCreateView, TrainingRetrieveUpdateDestroyView, ClientListCreateDestroyView, ClientRetrieveView
from notifications.models import Notification, NotificationStatus
from storage.models import StorageFile, MyFile


class MyAPITestCase(APITestCase):
    def authorize(self):
        self.client.force_login(self.user)

    def setUp(self):
        u1 = User.objects.create(username="asdasd-asdads-qweqwe", email="e@mail.com")
        u2 = User.objects.create(username="asdsad-asdads-rrewww", email="a@mail.com")
        User.objects.create(username="czxzcc-ewqqwe-qweqwe", email="z@mail.com")

        t1 = Tag.objects.create(title="Odchudzanie")
        t2 = Tag.objects.create(title="Zdrowie")
        t3 = Tag.objects.create(title="Siła")
        t4 = Tag.objects.create(title="Kondycja")
        self.user = User.objects.create(username="pgusnr-pqi82m1-ppvk42n",
                                        email="marcin@gmail.com",
                                        first_name="Marcin",
                                        last_name="")
        offer = Offer.objects.create(owner=self.user,
                                     description='Indywidualny trening',
                                     title='Trening',
                                     price_in_gr=None)
        Offer.objects.create(owner=u1,
                             description='Grupowy trening',
                             title='Trening 2',
                             price_in_gr=None)
        offer.tags.add(t1.id, t2.id)
        offer.save()
        tags = [t1, t2, t3, t4]
        for i in range(10):
            o = Offer.objects.create(title=f'Offer {i}', description='description', price_in_gr=i*100,
                                     price_description="/h", owner=self.user)
            for tag in range(1, 5):
                if i % tag == 0:
                    o.tags.add(tags[tag-1].id)

        b1 = BodyPart.objects.create(name="Nogi")
        b2 = BodyPart.objects.create(name="Ręce")
        b3 = BodyPart.objects.create(name="Plecy")
        b4 = BodyPart.objects.create(name="Brzuch")

        et1 = ExerciseType.objects.create(title='fitness')
        et2 = ExerciseType.objects.create(title='gimnastyka')
        et3 = ExerciseType.objects.create(title='siła')

        ep1 = ExercisePattern.objects.create(title='push up')
        ep2 = ExercisePattern.objects.create(title='pull up')
        ep3 = ExercisePattern.objects.create(title='pat3')

        e1 = Exercise.objects.create(name='pompki', description='pompki desc', owner=self.user)
        e1.body_parts.add(b2.id)
        e1.patterns.add(ep1.id, ep2.id)
        e1.types.add(et1.id, et3.id)
        e1.save()
        e2 = Exercise.objects.create(name='brzuszki', description='b desc', owner=self.user)
        e2.body_parts.add(b3.id)
        e3 = Exercise.objects.create(name='deska', description='d desc', owner=self.user)
        e3.body_parts.add(b1.id, b3.id, b4.id)
        e4 = Exercise.objects.create(name='pompki 2', description='pompki desc 2', owner=u1)
        e4.body_parts.add(b2.id)

        s = Set.objects.create(name="Trening brzucha", description="brzuch", owner=self.user)
        ExercisesSet.objects.create(set=s, exercise=e2, order=1, reps="20")
        ExercisesSet.objects.create(set=s, exercise=e3, order=2, reps="60 sekund")
        s = Set.objects.create(name="Trening ramion", description="ramiona", owner=self.user)
        ExercisesSet.objects.create(set=s, exercise=e1, order=1, reps="20")
        ExercisesSet.objects.create(set=s, exercise=e3, order=2, reps="15")
        ExercisesSet.objects.create(set=s, exercise=e2, order=3, reps="10")

        t = Training.objects.create(name="T1", description="Trening 1", owner=self.user)
        TrainingSets.objects.create(training=t, set=s, note="note", reps="2", order=1)
        Training.objects.create(name="T2", description="Trening 2", owner=self.user)
        TrainingExercises.objects.create(training=t, exercise=e1, note="Az", order=2, reps=3)

        TrainerClients.objects.create(trainer=self.user, client=u1)
        TrainerClients.objects.create(trainer=self.user, client=u2)

        f = MyFile.objects.create(url='some_url', public=True)
        StorageFile.objects.create(name='profilowe', type='profile', file=f, owner=self.user)

    def auth_request(self, url, view_class, method, data={}, pk=0):
        factory = APIRequestFactory()
        view = view_class.as_view()

        if method == "GET":
            request = factory.get(url, data=data, format='json')
        elif method == "POST":
            request = factory.post(url, data=data, format='json')
        elif method == "PATCH":
            request = factory.patch(url, data=data, format='json')
        elif method == "DELETE":
            request = factory.delete(url, data=data, format='json')
        elif method == "PUT":
            request = factory.put(url, data=data, format='json')

        force_authenticate(request, user=self.user)
        return view(request, pk=pk) if pk else view(request)


class UserTest(MyAPITestCase):
    def test_get_user_data(self):
        TrainerClients.objects.create(trainer=User.objects.first(), client=self.user)
        response = self.auth_request('/api/user/', UserSelfInfoView, "GET")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertIn('id', data)
        self.assertEqual(data['email'], self.user.email)
        self.assertEqual(data['first_name'], self.user.first_name)
        self.assertEqual(data['last_name'], self.user.last_name)
        self.assertEqual(data['profile_photo'], None)
        self.assertEqual(data['description'], '')
        self.assertEqual(data['certificates'], '')
        self.assertEqual(data['city'], '')
        self.assertEqual(data['tags'], [])
        self.assertIn('address', data)
        self.assertIn('birth_date', data)
        self.assertEqual(data['trainers'][0]['id'], User.objects.first().id)
        self.assertEqual(data['trainers'][0]['first_name'], User.objects.first().first_name)

    def test_get_other_user_data(self):
        data = self.client.get(f'/api/user/details/{self.user.id}/').data
        self.assertEqual(data['email'], self.user.email)
        self.assertEqual(data['first_name'], self.user.first_name)
        self.assertEqual(data['last_name'], self.user.last_name)
        self.assertEqual(data['profile_photo'], None)
        self.assertEqual(data['description'], '')
        self.assertEqual(data['certificates'], '')
        self.assertEqual(data['city'], '')
        self.assertEqual(data['tags'], [])
        self.assertEqual(data['type'], 3)
        self.assertNotIn('address', data)
        self.assertNotIn('birth_date', data)

    def test_update_user_data(self):
        req_data = {'first_name': "Oskar", 'email': 'cannotchangeemail@gmail.com', 'postal_code': '54-895'}
        response = self.auth_request('/api/user/', UserUpdateView, "PATCH", req_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(data['first_name'], req_data['first_name'])
        self.assertEqual(data['postal_code'], req_data['postal_code'])
        self.assertEqual(data['last_name'], self.user.last_name)
        self.user.refresh_from_db()
        self.assertEqual(self.user.first_name, req_data['first_name'])
        self.assertNotEqual(self.user.email, req_data['email'])

    def test_update_user_data_city(self):
        req_data = {'description': 'Lorem ipsum', 'city': "Sosnowiec"}
        response = self.auth_request('/api/user/', UserUpdateView, "PATCH", req_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = self.auth_request('/api/user/', UserSelfInfoView, "GET").data
        self.assertEqual(data['description'], 'Lorem ipsum')
        self.assertEqual(data['city'], "Sosnowiec")

    def test_update_user_data_tags(self):
        response = self.auth_request('/api/user/', UserUpdateView, "PATCH",
                                     {'tags': [Tag.objects.first().id, Tag.objects.all()[1].id]})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = self.auth_request('/api/user/', UserSelfInfoView, "GET").data
        self.assertEqual(data['tags'][0]['title'], Tag.objects.first().title)
        self.assertEqual(data['tags'][1]['title'], Tag.objects.all()[1].title)

    def test_update_user_bad_postal_code(self):
        req_data = {'postal_code': '50895'}
        response = self.auth_request('/api/user/', UserUpdateView, "PATCH", req_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_photo(self):
        req_data = {'profilePhoto': "some_url"}
        response = self.auth_request('/api/user/', UserUpdateView, "PATCH", req_data)
        data = response.data
        self.assertEqual('some_url', data['profile_photo']['url'])
        self.assertNotIn('expiresIn', data['profile_photo'])
        self.user.refresh_from_db()
        self.assertIsNotNone(self.user.profile_photo)
        req_data = {'profilePhoto': "some_url2"}
        response = self.auth_request('/api/user/', UserUpdateView, "PATCH", req_data)
        data = response.data
        self.assertEqual('some_url2', data['profile_photo']['url'])
        self.assertNotIn('expiresIn', data['profile_photo'])

    def test_get_user_type(self):
        response = self.auth_request('/api/user/', UserSelfInfoView, "GET")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['type'], 3)

    def test_user_type_update(self):
        response = self.auth_request('/api/user/', UserUpdateView, "PATCH", {'type': 2})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertEqual(self.user.get_type_display(), 'Dietetyk')

    def test_put_user(self):
        response = self.auth_request('/api/user/', UserUpdateView, "PUT")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_get_types(self):
        data = self.client.get('/api/user/types/').data
        self.assertEqual(data, [{'id': 1, 'name': 'Trener personalny'},
                                {'id': 2, 'name': 'Dietetyk'},
                                {'id': 3, 'name': 'Klient'}])

    def test_is_female(self):
        self.user.first_name = "Ewa"
        self.user.save()
        self.assertTrue(self.user.is_female)
        self.user.first_name = "Marcin"
        self.user.save()
        self.assertFalse(self.user.is_female)


class TagsTest(MyAPITestCase):
    def test_tags_get(self):
        response = self.client.get('/api/tags/')
        data = response.data
        self.assertEqual(len(data), 4)
        self.assertEqual(data[0]['title'], 'Kondycja')


class TestOffer(MyAPITestCase):
    def test_create_offer(self):
        data_to_send = {
            'description': 'Indywidualny trening',
            'title': 'Trening',
            'tags': [Tag.objects.first().id, Tag.objects.all()[1].id],
            'price_in_gr': 7999,
            'price_description': '/h',
            'id': 5000
        }
        response = self.auth_request("/api/offer/", OfferListCreateView, "POST", data_to_send)
        self.assertIn('id', response.data)
        self.assertNotEqual(response.data['id'], data_to_send['id'])
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        offer = Offer.objects.get(id=response.data['id'])
        self.assertEqual(offer.description, data_to_send['description'])
        self.assertEqual(offer.title, data_to_send['title'])
        self.assertEqual(offer.tags.count(), 2)
        self.assertEqual(offer.price_in_gr, data_to_send['price_in_gr'])
        self.assertEqual(offer.price_description, data_to_send['price_description'])
        self.assertEqual(offer.owner, self.user)
        self.assertTrue(offer.active)

    def test_retrieve_offer(self):
        offer = Offer.objects.first()
        data = self.client.get(f'/api/offer/{offer.id}/').data
        self.assertEqual(offer.description, data['description'])
        self.assertEqual(offer.title, data['title'])
        self.assertEqual(data['tags'][0]['id'], offer.tags.first().id)
        self.assertEqual(data['tags'][1]['id'], offer.tags.all()[1].id)
        self.assertEqual(len(data['tags']), 2)
        self.assertEqual(offer.active, data['active'])
        self.assertEqual(offer.price_description, data['price_description'])
        self.assertEqual(data['owner']['email'], self.user.email)
        self.assertEqual(data['owner']['first_name'], self.user.first_name)

    def test_update_offer(self):
        offer = Offer.objects.first()
        data_to_send = {
            'tags': [Tag.objects.all()[2].id],
            'owner__id': User.objects.all()[1].id,
            'price_in_gr': 10,
            'price_description': '/h',
            'active': False,
            'id': 5000
        }
        self.auth_request(f'/api/offer/{offer.id}/', OfferRetrieveUpdateDestroyView, "PATCH", data_to_send, offer.id)
        offer.refresh_from_db()
        self.assertEqual(offer.owner.id, self.user.id)
        self.assertEqual(offer.active, data_to_send['active'])
        self.assertEqual(offer.price_description, data_to_send['price_description'])
        self.assertEqual(offer.price_in_gr, data_to_send['price_in_gr'])
        self.assertEqual(offer.tags.count(), 1)
        self.assertEqual(offer.tags.first().id, Tag.objects.all()[2].id)
        self.assertNotEqual(offer.id, data_to_send['id'])

    def test_update_offer_no_perm(self):
        offer = Offer.objects.first()
        data_to_send = {
            'tags': [Tag.objects.all()[2].id],
            'owner__id': User.objects.all()[1].id,
            'price_in_gr': 10,
            'price_description': '/h',
            'active': False,
            'id': 5000
        }
        response = self.client.patch(f'/api/offer/{offer.id}/', data_to_send)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_offer(self):
        offer = Offer.objects.first()
        response = self.auth_request(f'/api/offer/{offer.id}/', OfferRetrieveUpdateDestroyView, 'DELETE', pk=offer.id)
        with self.assertRaises(Offer.DoesNotExist):
            offer.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_list_offers(self):
        data = self.auth_request('/api/offer/', OfferListCreateView, "GET").data
        self.assertEqual(len(data), 11)
        offer = data[0]
        self.assertEqual(offer['id'], Offer.objects.filter(owner=self.user).first().id)

    def test_filter_offers(self):
        data_to_send = {}
        res = self.client.get('/api/offer/filter/', data=data_to_send)
        data = res.data
        self.assertEqual(data['count'], 12)

    def test_filter_offers_owner(self):
        data_to_send = {
            'owner': self.user.id
        }
        res = self.client.get('/api/offer/filter/', data=data_to_send)
        data = res.data
        self.assertEqual(data['count'], 11)

    def test_filter_offers_text(self):
        data_to_send = {
            'text': 'trening'
        }
        res = self.client.get('/api/offer/filter/', data=data_to_send)
        data = res.data
        self.assertEqual(data['count'], 2)

    def test_filter_offers_price(self):
        data_to_send = {
            'min_price': 500,
            'max_price': 800
        }
        res = self.client.get('/api/offer/filter/', data=data_to_send)
        data = res.data
        self.assertEqual(data['count'], 4)

    def test_filter_offers_price_description(self):
        data_to_send = {
            'price_description': '/h'
        }
        res = self.client.get('/api/offer/filter/', data=data_to_send)
        data = res.data
        self.assertEqual(data['count'], 10)

    def test_filter_offers_tags(self):
        tags = Tag.objects.all().order_by('id')
        data_to_send = {
            'tags': [tags[0].id, tags[1].id, tags[2].id]
        }
        res = self.client.get('/api/offer/filter/', data=data_to_send)
        data = res.data
        self.assertEqual(data['count'], 2)

    def test_ordering_by_price(self):
        data_to_send = {
            'order': 'price_in_gr',
        }
        res = self.client.get('/api/offer/filter/', data=data_to_send)
        data = res.data['results']
        self.assertEqual(data[-1]['price_in_gr'], None)
        self.assertEqual(data[0]['price_in_gr'], 0)

    def test_ordering_by_price_desc(self):
        data_to_send = {
            'order': '-price_in_gr',
            'max_price': 300
        }
        res = self.client.get('/api/offer/filter/', data=data_to_send)
        data = res.data['results']
        self.assertEqual(data[0]['price_in_gr'], 300)


class TestExercisesStuff(MyAPITestCase):
    def test_create_exercise(self):
        data_to_send = {
            'name': 'pompki',
            'description': 'no pompki po prostu',
            'body_parts': [BodyPart.objects.first().id, BodyPart.objects.all()[1].id],
            'patterns': [ExercisePattern.objects.first().id, ExercisePattern.objects.all()[2].id],
            'types': [ExerciseType.objects.first().id]
        }
        response = self.auth_request('/api/exercise/', ExerciseListCreateView, "POST", data_to_send)
        self.assertEqual(response.status_code, 201)
        exercise = Exercise.objects.get(id=response.data['id'])
        self.assertEqual(exercise.owner, self.user)
        self.assertEqual(exercise.patterns.count(), 2)
        self.assertEqual(exercise.types.count(), 1)

    def test_update_exercise(self):
        exercise = Exercise.objects.create(name="pompki", description="pompeczki", owner=self.user)
        data_to_send = {
            'description': 'brzuchy',
            'body_parts': [BodyPart.objects.first().id],
            'patterns': [ExercisePattern.objects.first().id, ExercisePattern.objects.all()[1].id,
                         ExercisePattern.objects.all()[2].id],
            'media': StorageFile.objects.first().id
        }
        response = self.auth_request(f'/api/exercise/',
                                     ExerciseRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=exercise.id)

        self.assertEqual(response.status_code, 200)
        exercise.refresh_from_db()
        self.assertEqual(exercise.description, data_to_send['description'])
        self.assertEqual(exercise.body_parts.count(), 1)
        self.assertEqual(exercise.patterns.count(), 3)
        self.assertEqual(exercise.body_parts.first().id, data_to_send['body_parts'][0])
        self.assertEqual(exercise.media.id, StorageFile.objects.first().id)

    def test_update_exercise_no_perm(self):
        exercise = Exercise.objects.create(name="pompki", description="pompeczki", owner_id=User.objects.first().id)
        data_to_send = {
            'description': 'brzuchy'
        }
        response = self.auth_request(f'/api/exercise/',
                                     ExerciseRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=exercise.id)
        self.assertEqual(response.status_code, 403)

    def test_update_exercise_no_perm_media(self):
        exercise = Exercise.objects.create(name="pompki", description="pompeczki", owner=self.user)
        media = StorageFile.objects.first()
        media.owner = User.objects.first()
        media.save()

        data_to_send = {
            'media': media.id
        }

        response = self.auth_request(f'/api/exercise/',
                                     ExerciseRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=exercise.id)
        self.assertEqual(response.status_code, 400)

    def test_retrieve_exercise(self):
        exercise = Exercise.objects.create(name="pompki", description="pompeczki",
                                           media=StorageFile.objects.first(), owner=self.user)
        exercise.body_parts.add(BodyPart.objects.first().id, BodyPart.objects.all()[1].id)
        exercise.patterns.add(ExercisePattern.objects.first().id)
        exercise.types.add(ExerciseType.objects.all()[1].id)
        response = self.auth_request(f'/api/exercise/', ExerciseRetrieveUpdateDestroyView, "GET", pk=exercise.id)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], exercise.name)
        self.assertIn(response.data['media']['file']['url'], exercise.media.file.url)
        self.assertEqual(len(response.data['body_parts']), 2)
        self.assertEqual(len(response.data['patterns']), 1)
        self.assertEqual(len(response.data['types']), 1)
        self.assertEqual(response.data['types'][0]['id'], ExerciseType.objects.all()[1].id)
        self.assertEqual(response.data['patterns'][0]['id'], ExercisePattern.objects.first().id)
        self.assertIn('name', response.data['body_parts'][0])
        self.assertIn('title', response.data['patterns'][0])
        self.assertIn('title', response.data['types'][0])
        self.assertIn('name', response.data['body_parts'][0])

    def test_delete_exercise(self):
        exercise = Exercise.objects.create(name="pompki", description="pompeczki", owner=self.user)
        response = self.auth_request(f'/api/exercise/', ExerciseRetrieveUpdateDestroyView, "DELETE",
                                     pk=exercise.id)
        self.assertEqual(response.status_code, 204)
        with self.assertRaises(Exercise.DoesNotExist):
            exercise.refresh_from_db()

    def test_delete_exercise_no_perm(self):
        exercise = Exercise.objects.create(name="pompki", description="pompeczki", owner_id=User.objects.first().id)
        response = self.auth_request(f'/api/exercise/', ExerciseRetrieveUpdateDestroyView, "DELETE", pk=exercise.id)
        self.assertEqual(response.status_code, 403)
        Exercise.objects.get(id=exercise.id)

    def test_list_exercise(self):
        exercise = Exercise.objects.create(name="aaabrzuchy", description="brzuszki", owner=self.user)
        exercise.body_parts.add(BodyPart.objects.first().id, BodyPart.objects.all()[1].id)
        exercise.patterns.add(ExercisePattern.objects.first().id, ExercisePattern.objects.all()[1].id)
        exercise.types.add(ExerciseType.objects.first().id, ExerciseType.objects.all()[1].id)
        response = self.auth_request(f'/api/exercise/', ExerciseListCreateView, "GET")
        data = response.data
        self.assertEqual(len(data), 4)
        self.assertEqual(data[0]['name'], exercise.name)
        self.assertEqual(data[0]['media'], exercise.media)
        self.assertIn('name', data[0]['body_parts'][0])
        self.assertIn('title', data[0]['patterns'][0])
        self.assertIn('title', data[0]['types'][0])

    def test_list_body_part(self):
        response = self.client.get('/api/exercise/bodyparts/')
        self.assertEqual(len(response.data), BodyPart.objects.all().count())
        self.assertIn('id', response.data[0])
        self.assertIn('name', response.data[0])

    def test_list_exercise_types(self):
        response = self.client.get('/api/exercise/types/')
        self.assertEqual(len(response.data), ExerciseType.objects.all().count())
        self.assertIn('id', response.data[0])
        self.assertIn('title', response.data[0])

    def test_list_exercise_patterns(self):
        response = self.client.get('/api/exercise/patterns/')
        self.assertEqual(len(response.data), ExercisePattern.objects.all().count())
        self.assertIn('id', response.data[0])
        self.assertIn('title', response.data[0])

    def test_create_set(self):
        data_to_send = {
            'name': 'Biceps',
            'description': 'dla doświadczonych',
            'exercises': [{
                'exercise': Exercise.objects.first().id,
                'reps': 5,
                'order': 1
            }, {
                'exercise': Exercise.objects.all()[1].id,
                'reps': 10,
                'order': 2
            }, {
                'exercise': Exercise.objects.all()[2].id,
                'reps': 15,
                'order': 3
            }]
        }
        response = self.auth_request("/api/set/", SetListCreateView, "POST", data_to_send)
        set = Set.objects.get(pk=response.data['id'])
        self.assertEqual(set.description, data_to_send['description'])
        self.assertEqual(set.owner, self.user)
        self.assertEqual(set.exercises.count(), len(data_to_send['exercises']))

    def test_create_set_no_exercise_owner(self):
        e = Exercise.objects.all()[1]
        e.owner = User.objects.first()
        e.save()
        data_to_send = {
            'name': 'Biceps',
            'description': 'dla doświadczonych',
            'exercises': [{
                'exercise': Exercise.objects.first().id,
                'reps': 5,
                'order': 1
            }, {
                'exercise': e.id,
                'reps': 10,
                'order': 2
            }, {
                'exercise': Exercise.objects.all()[2].id,
                'reps': 15,
                'order': 3
            }]
        }
        response = self.auth_request("/api/set/", SetListCreateView, "POST", data_to_send)
        self.assertEqual(response.status_code, 400)

    def test_list_set(self):
        response = self.auth_request("/api/set/", SetListCreateView, "GET")
        self.assertEqual(len(response.data), Set.objects.filter(owner=self.user).count())
        set = response.data[0]
        set_db = Set.objects.get(pk=set['id'])
        self.assertEqual(set['description'], set_db.description)
        self.assertIn('reps', set['exercises'][0])
        self.assertIn('order', set['exercises'][0])
        self.assertIn('body_parts', set['exercises'][0])
        self.assertIn('name', set['exercises'][0]['body_parts'][0])

    def test_retrieve_set(self):
        set = Set.objects.first()
        response = self.auth_request("/api/set/", SetRetrieveUpdateDestroyView, "GET", pk=set.id)

        self.assertEqual(set.id, response.data['id'])
        self.assertEqual(set.description, response.data['description'])
        self.assertEqual(set.name, response.data['name'])
        self.assertEqual(set.exercises.count(), len(response.data['exercises']))
        self.assertEqual(set.exercises.first().reps, response.data['exercises'][0]['reps'])
        self.assertIn('name', response.data['exercises'][0]['body_parts'][0])

    def test_retrieve_set_no_perm(self):
        set = Set.objects.first()
        response = self.client.get(f"/api/set/{set.id}/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_set(self):
        set = Set.objects.first()
        response = self.auth_request("/api/set/", SetRetrieveUpdateDestroyView, "DELETE", pk=set.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(Set.DoesNotExist):
            set.refresh_from_db()

    def test_delete_no_perm(self):
        set = Set.objects.first()
        response = self.client.delete(f"/api/set/{set.id}/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_set_no_exercises(self):
        set = Set.objects.first()
        data_to_send = {
            'name': 'nowa nazwa'
        }
        response = self.auth_request("/api/set/", SetRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=set.id)
        new_set = Set.objects.get(pk=set.id)
        self.assertEqual(new_set.name, data_to_send['name'])
        self.assertEqual(set.exercises.count(), new_set.exercises.count())

    def test_update_set_empty_exercises(self):
        set = Set.objects.first()
        self.assertNotEqual(0, set.exercises.count())
        data_to_send = {
            'name': 'nowa nazwa',
            'exercises': []
        }
        response = self.auth_request("/api/set/", SetRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=set.id)
        set.refresh_from_db()
        self.assertEqual(set.name, data_to_send['name'])
        self.assertEqual(0, set.exercises.count())

    def test_update_set_exercises(self):
        set = Set.objects.first()
        self.assertNotEqual(0, set.exercises.count())
        data_to_send = {
            'description': 'nowa nazwa',
            'exercises': [{
                'exercise': Exercise.objects.first().id,
                'reps': '5',
                'order': 1
            }, {
                'exercise': Exercise.objects.all()[1].id,
                'reps': '15',
                'order': 2
            }, {
                'exercise': Exercise.objects.all()[2].id,
                'reps': '15',
                'order': 3,
            }, {
                'exercise': Exercise.objects.first().id,
                'reps': '5',
                'order': 4
            }]
        }
        response = self.auth_request("/api/set/", SetRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=set.id)
        set.refresh_from_db()
        self.assertEqual(set.description, data_to_send['description'])
        self.assertEqual(set.exercises.count(), len(data_to_send['exercises']))
        self.assertEqual(set.exercises.first().reps, data_to_send['exercises'][0]['reps'])
        self.assertIn('name', response.data['exercises'][0]['body_parts'][0])

    def test_training_create(self):
        data_to_send = {
            'name': 'Poniedziałek',
            'description': 'Trening poniedziałkowy',
            'exercises': [{
                'id': Exercise.objects.first().id,
                'reps': '5',
                'order': 1
            }, {
                'id': Exercise.objects.all()[1].id,
                'reps': '15',
                'order': 3
            }],
            'sets': [{
                'id': Set.objects.first().id,
                'reps': '1',
                'order': 2,
                'note': "mogą być dwie serie"
            }]
        }
        response = self.auth_request("/api/training/", TrainingListCreateView, "POST", data_to_send)
        self.assertIn('id', response.data)
        training = Training.objects.get(id=response.data['id'])
        self.assertEqual(training.sets.count(), len(data_to_send['sets']))
        self.assertEqual(training.exercises.count(), len(data_to_send['exercises']))
        self.assertIn('order', response.data['sets'][0])
        self.assertIn('name', response.data['sets'][0]['exercises'][0]['body_parts'][0])

    def test_training_create_no_perm(self):
        exercise = Exercise.objects.first()
        exercise.owner = User.objects.first()
        exercise.save()
        data_to_send = {
            'name': 'Poniedziałek',
            'description': 'Trening poniedziałkowy',
            'exercises': [{
                'id': exercise.id,
                'reps': '5',
                'order': 1
            }],
            'sets': [{
                'id': Set.objects.first().id,
                'reps': '1',
                'order': 2,
                'note': "mogą być dwie serie"
            }]
        }
        response = self.auth_request("/api/training/", TrainingListCreateView, "POST", data_to_send)
        self.assertEqual(response.status_code, 400)

    def test_training_create_no_set_perm(self):
        set = Set.objects.first()
        set.owner = User.objects.first()
        set.save()
        data_to_send = {
            'name': 'Poniedziałek',
            'description': 'Trening poniedziałkowy',
            'sets': [{
                'id': set.id,
                'reps': '1',
                'order': 1,
                'note': "mogą być dwie serie"
            }]
        }
        response = self.auth_request("/api/training/", TrainingListCreateView, "POST", data_to_send)
        self.assertEqual(response.status_code, 400)

    def test_training_list(self):
        response = self.auth_request("/api/training/", TrainingListCreateView, "GET")
        data = response.data
        self.assertEqual(len(data), Training.objects.filter(owner=self.user).count())
        self.assertIn('name', data[0]['sets'][0]['exercises'][0]['body_parts'][0])

    def test_training_delete(self):
        training = Training.objects.filter(owner=self.user).first()
        self.auth_request("/api/training/", TrainingRetrieveUpdateDestroyView, "DELETE", pk=training.id)
        with self.assertRaises(Training.DoesNotExist):
            training.refresh_from_db()

    def test_training_delete_no_perm(self):
        response = self.client.delete(f"/api/training/{Training.objects.first().id}/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_training_get(self):
        training = Training.objects.filter(owner=self.user).first()
        response = self.auth_request("/api/training/", TrainingRetrieveUpdateDestroyView, "GET", pk=training.id)
        self.assertEqual(response.data['name'], training.name)
        self.assertEqual(response.data['description'], training.description)
        self.assertEqual(len(response.data['exercises']), training.exercises.count())
        self.assertIn('name', response.data['sets'][0]['exercises'][0]['body_parts'][0])

    def test_training_get_no_perm(self):
        training = Training.objects.first()
        training.owner = User.objects.first()
        training.save()
        response = self.auth_request("/api/training/", TrainingRetrieveUpdateDestroyView, "GET", pk=training.id)
        self.assertEqual(response.status_code, 403)

    def test_training_get_for_clients(self):
        training = Training.objects.first()
        training.owner = User.objects.first()
        training.save()
        TrainerClients.objects.create(trainer=training.owner, client=self.user)

        response = self.auth_request("/api/training/", TrainingRetrieveUpdateDestroyView, "GET", pk=training.id)
        self.assertEqual(response.status_code, 200)

    def test_training_patch(self):
        training = Training.objects.filter(owner=self.user).first()
        data_to_send = {
            'exercises': [{
                'id': Exercise.objects.first().id,
                'reps': '15',
                'order': 1
            }],
            'sets': [{
                'id': Set.objects.first().id,
                'reps': '15896',
                'order': 2,
                'note': "mogą być dwie serie"
            }]
        }
        response = self.auth_request("/api/", TrainingRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=training.id)
        data = response.data
        self.assertEqual(data['name'], training.name)
        self.assertEqual(data['description'], training.description)
        training.refresh_from_db()
        self.assertEqual(training.sets.first().reps, data_to_send['sets'][0]['reps'])
        self.assertEqual(training.exercises.count(), len(data_to_send['exercises']))


class TestClientManagement(MyAPITestCase):
    def setUp(self):
        super().setUp()
        self.user.type = 1      # trainer
        self.user.save()

    def test_add_client(self):
        client = User.objects.create(email="client@gmail.com")
        data_to_send = {
            'email': client.email
        }
        clients_num = self.user.clients.count()

        response = self.auth_request("/api/clients/", ClientListCreateDestroyView, "POST", data_to_send)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.user.refresh_from_db()
        self.assertEqual(self.user.clients.count(), clients_num + 1)
        self.assertEqual(self.user.clients.last().client.id, client.id)

        notification = Notification.objects.order_by('-id')[1]
        self.assertEqual(notification.title, "Masz nowego trenera!")
        self.assertEqual(notification.content, "Marcin  dodał Cię do swoich podopiecznych.")
        self.assertEqual(NotificationStatus.objects.filter(notification=notification).count(), 1)
        self.assertEqual(NotificationStatus.objects.filter(notification=notification).last().owner, client)
        notification = Notification.objects.last()
        self.assertEqual(notification.title, "Masz nowego klienta!")
        self.assertEqual(notification.content, f"{client.first_name} {client.last_name} został twoim podopiecznym.")
        self.assertEqual(NotificationStatus.objects.filter(notification=notification).count(), 1)
        self.assertEqual(NotificationStatus.objects.filter(notification=notification).last().owner, self.user)

    def test_add_client_not_trainer(self):
        self.user.type = 3      # trainer
        self.user.save()

        client = User.objects.create(email="client@gmail.com")
        data_to_send = {
            'email': client.email
        }
        response = self.auth_request("/api/clients/", ClientListCreateDestroyView, "POST", data_to_send)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_client(self):
        client = User.objects.create(email="client@gmail.com")
        data_to_send = {
            'email': client.email
        }
        response = self.auth_request("/api/clients/", ClientListCreateDestroyView, "POST", data_to_send)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.user.refresh_from_db()
        clients_num = self.user.clients.count()
        data_to_send = {
            'client': client.id
        }
        response = self.auth_request("/api/clients/", ClientListCreateDestroyView, "DELETE", data_to_send)
        self.user.refresh_from_db()
        self.assertEqual(self.user.clients.count(), clients_num - 1)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        notification = Notification.objects.last()
        self.assertEqual(notification.title, "Tracisz trenera")
        self.assertEqual(notification.content, "Marcin  usunął Cię z listy swoich podopiecznych.")
        self.assertEqual(NotificationStatus.objects.filter(notification=notification).count(), 1)
        self.assertEqual(NotificationStatus.objects.last().owner, client)

    def test_add_client_not_exists(self):
        data_to_send = {
            'email': 'ererere@gmail.com'
        }
        response = self.auth_request("/api/clients/", ClientListCreateDestroyView, "POST", data_to_send)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        invitation = TrainerInvitation.objects.get(client_email=data_to_send['email'], trainer=self.user)
        new_user = User.objects.create_user('', data_to_send['email'], '', '')
        with self.assertRaises(TrainerInvitation.DoesNotExist):
            invitation.refresh_from_db()
        TrainerClients.objects.get(client=new_user, trainer=self.user)

    def test_clients_get(self):
        response = self.auth_request("api/clients/", ClientListCreateDestroyView, "GET")
        self.assertEqual(len(response.data), self.user.clients.count())
        self.assertIn('first_name', response.data[0])
        self.assertNotIn('city', response.data[0])

    def test_client_get_details(self):
        client = TrainerClients.objects.filter(trainer=self.user).first().client
        client.height = 180
        client.birth_date = datetime.datetime(2000, 7, 30)
        client.save()
        response = self.auth_request("api/clients/", ClientRetrieveView, "GET", pk=client.id)
        self.assertEqual(response.data['height'], client.height)
        self.assertEqual(response.data['age'], 21)
