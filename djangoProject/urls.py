from django.urls import path, include
from django.conf import settings
from django.views.generic import TemplateView

urlpatterns = [
    path('api/', include('fituu.urls')),
    path('chat/', include('chat.urls')),
    path('storage/', include('storage.urls')),
    path('notifications/', include('notifications.urls')),
    path('calendar/', include('schedule.urls')),
    path('payments/', include('payments.urls')),
]

if settings.DEBUG:
    urlpatterns.append(path('', TemplateView.as_view(
        template_name='swagger.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger-ui'))
    urlpatterns.append(path('docs/redoc/', TemplateView.as_view(
        template_name='redoc.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='redoc-ui'))
