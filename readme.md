# Fituu 

API for an application for personal trainers.

## Deployment
Service is <b>no longer available</b> at https://fituu-backend-t84ol.ondigitalocean.app/

## Authorization

Create fituu account [here](https://fituu-dev.auth.us-east-2.amazoncognito.com/login?client_id=7itjkorqi942l44f6nsf9a1c3e&response_type=code&scope=email+openid+profile&redirect_uri=http://localhost:3000/api/auth/callback/cognito)

API server authorize requests using JWT access token from authorization header

Authorization=Bearer {access_token}

Access token can be generated from https://fituu-dev.auth.us-east-2.amazoncognito.com/oauth2/token

## API Documentation

API Documentation is available [here](https://gitlab.com/mgieron2012/fituu-backend/-/blob/master/static/docs.json).
