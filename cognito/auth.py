from rest_framework.exceptions import APIException

from djangoProject import settings
from fituu.models import User
from rest_framework import authentication
from rest_framework import exceptions
import jwt
import requests


public_key = b'''-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5eAth8NHOwqkaDjZYFD2
wkR+TbdXAo9XmHGYZyEVZL4UVbisPzV1B0LisuemjsGIcwDKTodGzqIUkmAg98RG
In85q1IKeBkmOAI9ZwLP4igHe5MIpnqmqRq5U6ijbVrH8/LxPE0NIvu2GMNaOAD/
hxLPzfNI2YETKk+X4rqEFpFNiO30YacVTovffq3VYwwOOH60AA2MYJgYfkxGbA24
QsPgLVMov52/yqxoCnKccRvOP2+TmvpjL73g+1eKnUUtPy5UarahVXBsvt4jJ4lN
XYkjklGvk3cWek41qutMHjsW3APV1R9iOrKbYjsYPHSeb+O+ZPWwz+J2Y1YQb0eE
7QIDAQAB
-----END PUBLIC KEY-----'''

app_client_id = "7itjkorqi942l44f6nsf9a1c3e"

kid = "4N2qC2mVgX7V9i9YtUwffvVk7FdLtyUILo5a5L1Ojkk="
# from https://cognito-idp.us-east-2.amazonaws.com/us-east-2_FEd0P35wJ/.well-known/jwks.json

token_iss = "https://cognito-idp.us-east-2.amazonaws.com/us-east-2_FEd0P35wJ"
# https://cognito-idp.us-east-2.amazonaws.com/<userpoolID>

userinfo_url = "https://fituu-dev.auth.us-east-2.amazoncognito.com/oauth2/userInfo"


class CognitoAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        try:
            token = retrieve_token_from_request(request)
        except (TypeError, AttributeError):
            return None, None
        if settings.DEBUG and token == "DOCS":
            return User.objects.get_or_create(username="docs", defaults={'username': 'docs'})
        if not verify_token_headers(token):
            raise IncorrectTokenException
        try:
            decoded = jwt.decode(token, public_key, algorithms=["RS256"])
        except jwt.ExpiredSignatureError:
            raise TokenExpiredException
        except Exception as e:
            print(e)
            raise IncorrectTokenException('Unknown error: ' + str(e))

        if not verify_token_payloads(decoded):
            raise IncorrectTokenException('JWT is not access token or not created by cognito')

        username = decoded.get("username")

        if not username:
            raise IncorrectTokenException('Username not in token data')

        try:
            user = User.objects.get(username=username)
            if not user.is_active:
                user_info = retrieve_user_info(token)
                if user_info.get("email_verified") == "true":
                    user.is_active = True
                    user.save()
                else:
                    raise EmailNotVerifiedException
            return user, None
        except User.DoesNotExist:
            user_info = retrieve_user_info(token)
            if user_info:
                user = User.objects.create_user(username,
                                                user_info.get("email", ""),
                                                user_info.get("first_name", ""),
                                                user_info.get("last_name", ""))
                return user, None
            else:
                raise exceptions.AuthenticationFailed('Could not retrieve user info from AWS cognito')


def retrieve_token_from_request(request):
    return request.META.get('HTTP_AUTHORIZATION').split()[1]


def verify_token_headers(token):
    headers = jwt.get_unverified_header(token)
    return headers.get("kid") == kid and headers.get("alg") == "RS256"


def verify_token_payloads(decoded_token):
    return decoded_token.get("token_use") == "access" and decoded_token.get("iss") == token_iss


def retrieve_user_info(token):
    headers = {'Authorization': f'Bearer {token}'}
    res = requests.get(userinfo_url, headers=headers)
    if res.ok:
        return res.json()


class IncorrectTokenException(APIException):
    status_code = 460
    default_detail = 'Incorrect token.'


class TokenExpiredException(APIException):
    status_code = 461
    default_detail = 'Token expired.'


class EmailNotVerifiedException(APIException):
    status_code = 462
    default_detail = 'Email not verified.'


class CognitoException(APIException):
    status_code = 550
    default_detail = 'Could not retrieve user info from AWS cognito'
