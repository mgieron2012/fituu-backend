from django.urls import path
from schedule.views import EventListCreateView, EventRetrieveUpdateDestroyView

urlpatterns = [
    path('', EventListCreateView.as_view()),
    path('<int:pk>/', EventRetrieveUpdateDestroyView.as_view())
]
