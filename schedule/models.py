from django.contrib.postgres.fields import ArrayField
from django.db import models

from fituu.models import User, Training


class Event(models.Model):
    id = models.AutoField(primary_key=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="created_events", null=True)
    title = models.CharField(max_length=150)
    note = models.TextField()
    place = models.CharField(max_length=200, default='')
    from_time = models.TimeField()
    to_time = models.TimeField()
    from_date = models.DateField()
    to_date = models.DateField()
    participants = models.ManyToManyField(User, related_name="events")
    interval = models.IntegerField(default=1)
    weekdays = ArrayField(models.IntegerField(), max_length=7)
    training = models.ForeignKey(Training, on_delete=models.SET_NULL, null=True)
