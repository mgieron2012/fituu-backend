from rest_framework import status

from fituu.tests import MyAPITestCase
from fituu.models import Training, User
from datetime import datetime, timedelta

from notifications.models import Notification, NotificationStatus
from schedule.models import Event
from schedule.views import EventListCreateView, EventRetrieveUpdateDestroyView
from schedule.utils import calculate_last_occurrence


class TestSchedule(MyAPITestCase):
    def setUp(self):
        super().setUp()
        self.event = Event.objects.create(owner=self.user, title='trening', note='moc', place='dom',
                                          from_time=datetime(2021, 8, 6, hour=10, minute=30).time(),
                                          to_time=datetime(2021, 8, 6, hour=12, minute=0).time(),
                                          from_date=datetime(2021, 8, 6).date(),
                                          to_date=datetime(2021, 12, 6).date(),
                                          interval=2, weekdays=[0, 2, 4], training=Training.objects.first())
        self.event.participants.add(User.objects.first(), self.user)

    def test_calculate_last_occurrence(self):
        self.assertEqual(calculate_last_occurrence(datetime(2021, 8, 6), [4], 1, 1), datetime(2021, 8, 6))
        self.assertEqual(calculate_last_occurrence(datetime(2021, 8, 6), [4], 2, 1), datetime(2021, 8, 13))
        self.assertEqual(calculate_last_occurrence(datetime(2021, 8, 6), [4], 2, 2), datetime(2021, 8, 20))
        self.assertEqual(calculate_last_occurrence(datetime(2021, 8, 6), [4, 5], 2, 2), datetime(2021, 8, 7))
        self.assertEqual(calculate_last_occurrence(datetime(2021, 8, 6), [3, 4], 2, 2), datetime(2021, 8, 12))
        self.assertEqual(calculate_last_occurrence(datetime(2021, 8, 6), [3, 4], 3, 2), datetime(2021, 8, 20))
        self.assertEqual(calculate_last_occurrence(datetime(2021, 8, 6), [4, 5], 7, 3), datetime(2021, 10, 8))

    def test_create_single_event(self):
        data_to_send = {
            'title': 'Trening nogi',
            'note': 'Moc',
            'time': {
                'from': datetime(2021, 8, 6, hour=10, minute=30).time(),
                'to': datetime(2021, 8, 6, hour=12, minute=0).time()
            },
            'date': datetime(2021, 8, 6).date(),
            'participants': [self.user.id],
            'place': 'Siłownia',
            'recurrence': None,
            'training': Training.objects.first().id
        }

        response = self.auth_request('some_url', EventListCreateView, "POST", data_to_send)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.data
        self.assertEqual(data['title'], data_to_send['title'])
        self.assertEqual(data['time']['from'], data_to_send['time']['from'])
        self.assertEqual(data['time']['to'], data_to_send['time']['to'])
        self.assertNotIn('date', data)
        self.assertEqual(data['place'], data_to_send['place'])
        self.assertEqual(data['note'], data_to_send['note'])
        self.assertIn('id', data)
        event = Event.objects.get(id=data['id'])
        self.assertEqual(data['place'], event.place)
        self.assertEqual(data['single'], True)

        self.assertEqual(event.owner, self.user)
        self.assertEqual(event.from_date, data_to_send['date'])
        self.assertEqual(event.to_date, data_to_send['date'])
        self.assertEqual(event.from_time, data_to_send['time']['from'])
        self.assertEqual(event.to_time, data_to_send['time']['to'])
        self.assertEqual(event.participants.count(), len(data_to_send['participants']))
        self.assertEqual(event.participants.first().id, data_to_send['participants'][0])
        self.assertEqual(len(event.weekdays), 1)
        self.assertEqual(event.weekdays[0], 4)
        self.assertEqual(event.interval, 1)
        self.assertEqual(event.training, Training.objects.first())

    def test_create_event_with_occurrences(self):
        data_to_send = {
            'title': 'Trening nogi',
            'note': 'Moc',
            'time': {
                'from': datetime(2021, 8, 6, hour=10, minute=30).time(),
                'to': datetime(2021, 8, 6, hour=12, minute=0).time()
            },
            'date': datetime(2021, 8, 6).date(),
            'participants': [self.user.id],
            'place': 'Siłownia',
            'recurrence': {
                'occurrences': 3,
                'interval': 2
            }
        }

        response = self.auth_request('some_url', EventListCreateView, "POST", data_to_send)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.data
        self.assertEqual(data['title'], data_to_send['title'])
        self.assertEqual(data['time']['from'], data_to_send['time']['from'])
        self.assertEqual(data['time']['to'], data_to_send['time']['to'])
        self.assertNotIn('date', data)
        self.assertEqual(data['place'], data_to_send['place'])
        self.assertEqual(data['note'], data_to_send['note'])
        self.assertIn('id', data)
        event = Event.objects.get(id=data['id'])
        self.assertEqual(data['place'], event.place)
        self.assertEqual(event.owner, self.user)
        self.assertEqual(event.from_date, data_to_send['date'])
        self.assertEqual(event.to_date, data_to_send['date'] + timedelta(days=28))
        self.assertEqual(len(event.weekdays), 1)
        self.assertEqual(event.weekdays[0], 4)
        self.assertEqual(event.interval, 2)
        self.assertEqual(event.training, None)

    def test_create_event_with_occurrences_and_weekdays(self):
        data_to_send = {
            'title': 'Trening nogi',
            'note': 'Moc',
            'time': {
                'from': datetime(2021, 8, 6, hour=10, minute=30).time(),
                'to': datetime(2021, 8, 6, hour=12, minute=0).time()
            },
            'date': datetime(2021, 8, 6).date(),
            'participants': [self.user.id],
            'place': 'Siłownia',
            'recurrence': {
                'occurrences': 2,
                'interval': 2,
                'weekdays': [0, 1, 2, 3, 5, 6]
            }
        }

        response = self.auth_request('some_url', EventListCreateView, "POST", data_to_send)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.data
        self.assertIn('id', data)
        event = Event.objects.get(id=data['id'])
        self.assertEqual(data['place'], event.place)
        self.assertEqual(event.owner, self.user)
        self.assertEqual(event.from_date, data_to_send['date'])
        self.assertEqual(event.to_date, data_to_send['date'] + timedelta(days=2))

    def test_create_event_with_last_event_date(self):
        data_to_send = {
            'title': 'Trening nogi',
            'note': 'Moc',
            'time': {
                'from': datetime(2021, 8, 6, hour=10, minute=30).time(),
                'to': datetime(2021, 8, 6, hour=12, minute=0).time()
            },
            'date': datetime(2021, 8, 6).date(),
            'participants': [self.user.id],
            'place': 'Siłownia',
            'recurrence': {
                'occurrences': None,
                'interval': 2,
                'weekdays': [0, 1, 2, 3, 4],
                'last_event_date': datetime(2021, 9, 6).date()
            }
        }

        response = self.auth_request('some_url', EventListCreateView, "POST", data_to_send)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.data
        self.assertIn('id', data)
        event = Event.objects.get(id=data['id'])
        self.assertEqual(data['place'], event.place)
        self.assertEqual(data['single'], False)
        self.assertEqual(event.owner, self.user)
        self.assertEqual(event.from_date, data_to_send['date'])
        self.assertEqual(event.to_date, data_to_send['schedule']['last_event_date'])

    def test_create_event_with_last_event_date(self):
        data_to_send = {
            'title': 'Trening nogi',
            'note': 'Moc',
            'time': {
                'from': datetime(2021, 8, 6, hour=10, minute=30).time(),
                'to': datetime(2021, 8, 6, hour=12, minute=0).time()
            },
            'date': datetime(2021, 8, 6).date(),
            'participants': [self.user.id],
            'place': 'Siłownia',
            'recurrence': {}
        }

        response = self.auth_request('some_url', EventListCreateView, "POST", data_to_send)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.data
        self.assertIn('id', data)
        event = Event.objects.get(id=data['id'])
        self.assertEqual(data['single'], False)
        self.assertEqual(event.from_date, data_to_send['date'])
        self.assertEqual(event.to_date, datetime.max.date())
        self.assertEqual(event.interval, 1)
        self.assertEqual(event.weekdays, [4])

    def test_create_event_notification(self):
        data_to_send = {
            'title': 'Trening nogi',
            'note': 'Moc',
            'time': {
                'from': datetime(2021, 8, 6, hour=10, minute=30).time(),
                'to': datetime(2021, 8, 6, hour=12, minute=0).time()
            },
            'date': datetime(2021, 8, 6).date(),
            'participants': [self.user.id, User.objects.first().id],
            'place': 'Siłownia',
            'recurrence': {}
        }
        self.user.first_name = "Marcin"
        self.user.last_name = "Nowak"
        self.user.save()
        self.auth_request('some_url', EventListCreateView, "POST", data_to_send)
        status = NotificationStatus.objects.last()
        status.owner = User.objects.first()
        notification = status.notification
        self.assertEqual(status.owner, User.objects.first())
        with self.assertRaises(NotificationStatus.DoesNotExist):
            NotificationStatus.objects.get(owner=self.user, notification=notification)
        self.assertEqual(notification.title, 'Zaplanowano wydarzenie')
        self.assertEqual(notification.content,
                         'Marcin Nowak zaplanował wydarzenie Trening nogi. Zajrzyj do swojego kalendarza!')

    def test_update_event_only(self):
        data_to_send = {
            'title': 'Trening łapy',
            'place': 'Siłownia',
            'date': datetime(2021, 9, 12).date(),
            'weekdays': [0, 1, 2, 3],
            'update': {
                'recurrence': 'no',
                'date': datetime(2021, 9, 10).date(),
            }
        }
        response = self.auth_request('url', EventRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=self.event.id)
        data = response.data

        events = Event.objects.order_by('from_date', 'to_date')
        self.assertEqual(events.first().from_date, self.event.from_date)
        self.assertEqual(events.first().to_date, data_to_send['update']['date'] - timedelta(days=1))
        self.assertEqual(events[1].from_date, data_to_send['update']['date'] + timedelta(days=1))
        self.assertEqual(events[1].to_date, self.event.to_date)
        self.assertEqual(events[2].from_date, data_to_send['date'])
        self.assertEqual(events[2].to_date, data_to_send['date'])

        self.assertEqual(events[0].title, events[1].title)
        self.assertNotEqual(events[0].title, data_to_send['title'])
        self.assertEqual(events[2].title, data_to_send['title'])
        self.assertEqual(events[2].place, data_to_send['place'])
        self.assertEqual(events[2].place, data['place'])

    def test_update_event_all(self):
        data_to_send = {
            'title': 'Trening łapy',
            'place': 'Siłownia',
            'date': datetime(2021, 9, 12).date(),
            'update': {
                'recurrence': 'all',
                'date': datetime(2021, 9, 10).date(),
            },
            'participants': [User.objects.first().id],
            'recurrence': {
                'weekdays': [6],
                'last_event_date': datetime(2021, 11, 10).date()
            }
        }

        response = self.auth_request('url', EventRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=self.event.id)
        data = response.data

        self.event.refresh_from_db()
        self.assertEqual(self.event.title, data_to_send['title'])
        self.assertEqual(self.event.to_date, data_to_send['recurrence']['last_event_date'])
        self.assertEqual(len(self.event.weekdays), len(data_to_send['recurrence']['weekdays']))
        self.assertEqual(self.event.weekdays[0], data_to_send['recurrence']['weekdays'][0])
        self.assertEqual(self.event.participants.first(), User.objects.first())
        self.assertEqual(self.event.participants.count(), len(data_to_send['participants']))

    def test_update_event_next(self):
        data_to_send = {
            'title': 'Trening łapy',
            'update': {
                'recurrence': 'next',
                'date': datetime(2021, 9, 10).date(),
            },
            'recurrence': {
                'last_event_date': datetime(2021, 11, 10).date()
            },
            'training': Training.objects.first().id
        }

        response = self.auth_request('url', EventRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=self.event.id)
        data = response.data

        events = Event.objects.order_by('from_date')
        self.assertEqual(events.first().from_date, self.event.from_date)
        self.assertEqual(events.first().to_date, data_to_send['update']['date'] - timedelta(days=1))
        self.assertEqual(events.last().from_date, data_to_send['update']['date'])
        self.assertEqual(events.last().to_date, data_to_send['recurrence']['last_event_date'])
        self.assertEqual(events.last().training, Training.objects.first())
        self.assertEqual(events.first().title, self.event.title)
        self.assertEqual(events.last().title, data_to_send['title'])

    def test_update_event_notification(self):
        data_to_send = {
            'title': 'Trening łapy',
            'update': {
                'recurrence': 'next',
                'date': datetime(2021, 9, 10).date(),
            }
        }
        self.auth_request('url', EventRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=self.event.id)

        notification = Notification.objects.last()
        self.assertEqual(notification.title, "Edytowano wydarzenie")
        self.assertEqual(notification.content, f"Marcin  wprowadził zmiany w wydarzeniu Trening łapy. "
                                               f"Sprawdź zmiany w swoim kalendarzu!")

    def test_update_event_no_perm(self):
        data_to_send = {
            'title': 'Trening łapy',
        }
        self.event.owner = User.objects.first()
        self.event.save()
        response = self.auth_request('url', EventRetrieveUpdateDestroyView, "PATCH", data_to_send, pk=self.event.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_events(self):
        data_to_send = {
            'from': datetime(2021, 8, 16).date(),
            'to': datetime(2021, 8, 29).date(),
        }
        response = self.auth_request('url', EventListCreateView, "GET", data_to_send)
        data = response.data
        self.assertEqual(len(data), 14)
        self.assertEqual(data[0]['date'], data_to_send['from'])
        self.assertEqual(data[13]['date'], data_to_send['to'])
        for day in data:
            if day['date'].day in [20, 23, 25]:
                self.assertEqual(len(day['events']), 1)
                self.assertEqual(day['events'][0]['id'], self.event.id)
            else:
                self.assertEqual(len(day['events']), 0)

    def test_list_events_only_participants(self):
        data_to_send = {
            'from': datetime(2021, 8, 16).date(),
            'to': datetime(2021, 8, 29).date(),
            'required_participation': True
        }
        self.event.participants.remove(self.user)
        self.event.save()

        response = self.auth_request('url', EventListCreateView, "GET", data_to_send)
        data = response.data
        self.assertEqual(len(data), 14)
        for day in data:
            self.assertEqual(len(day['events']), 0)

    def test_list_events_no_params(self):
        data_to_send = {
            'from': datetime(2021, 8, 16).date()
        }

        response = self.auth_request('url', EventListCreateView, "GET", data_to_send)
        self.assertEqual(response.status_code, 400)

    def test_retrieve_event(self):
        response = self.auth_request('url', EventRetrieveUpdateDestroyView, "GET", pk=self.event.pk)
        data = response.data
        self.assertEqual(data['title'], self.event.title)
        self.assertEqual(data['participants'][0]['first_name'], self.event.participants.first().first_name)
        self.assertEqual(data['training'], self.event.training_id)

    def test_delete_event_all(self):
        data_to_send = {
            'date': datetime(2021, 8, 16).date(),
            'recurrence': "all",
        }
        self.auth_request('url', EventRetrieveUpdateDestroyView, "DELETE", data_to_send, pk=self.event.pk)
        with self.assertRaises(Event.DoesNotExist):
            self.event.refresh_from_db()

        notification = Notification.objects.last()
        self.assertEqual(notification.title, "Odwołano wydarzenie")
        self.assertEqual(notification.content, f"{self.user.first_name} {self.user.last_name} odwołał wydarzenie "
                                               f"{self.event.title}.")

    def test_delete_event_next(self):
        data_to_send = {
            'date': datetime(2021, 9, 16).date(),
            'recurrence': "next",
        }
        self.auth_request('url', EventRetrieveUpdateDestroyView, "DELETE", data_to_send, pk=self.event.pk)
        self.event.refresh_from_db()
        self.assertEqual(self.event.to_date, data_to_send['date'] - timedelta(days=1))
        self.assertEqual(Event.objects.count(), 1)

        notification = Notification.objects.last()
        self.assertEqual(notification.title, "Odwołano wydarzenie")
        self.assertEqual(notification.content, f"{self.user.first_name} {self.user.last_name} odwołał wydarzenie "
                                               f"{self.event.title} odbywające się w dniu 16.09.2021 i później.")

    def test_delete_event_one(self):
        data_to_send = {
            'date': datetime(2021, 9, 16).date(),
            'recurrence': "no",
        }
        self.auth_request('url', EventRetrieveUpdateDestroyView, "DELETE", data_to_send, pk=self.event.pk)

        self.assertEqual(Event.objects.order_by('from_date').last().from_date, data_to_send['date'] + timedelta(days=1))
        self.assertEqual(Event.objects.last().to_date, self.event.to_date)
        self.event.refresh_from_db()
        self.assertEqual(self.event.to_date, data_to_send['date'] - timedelta(days=1))
        self.assertEqual(Event.objects.count(), 2)

        notification = Notification.objects.last()
        self.assertEqual(notification.title, "Odwołano wydarzenie")
        self.assertEqual(notification.content, f"{self.user.first_name} {self.user.last_name} odwołał wydarzenie "
                                               f"{self.event.title} odbywające się w dniu 16.09.2021.")

    def test_delete_event_no_perm(self):
        data_to_send = {
            'date': datetime(2021, 8, 16).date(),
            'recurrence': "all",
        }
        self.event.owner = User.objects.first()
        self.event.save()
        response = self.auth_request('url', EventRetrieveUpdateDestroyView, "DELETE", data_to_send, pk=self.event.pk)
        self.assertEqual(response.status_code, 403)
