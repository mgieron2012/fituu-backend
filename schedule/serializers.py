from copy import copy
from datetime import datetime, timedelta

from rest_framework import serializers

from fituu.models import Training
from fituu.serializers import UserSimpleSerializer
from fituu.utils import in_data_or_raise_validation_error
from schedule.models import Event
from schedule.utils import calculate_last_occurrence, str_to_date, str_to_time


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = "__all__"

    def is_single_event(self, instance):
        return instance.from_date == instance.to_date

    def to_internal_value(self, data):
        if time := data.pop('time', None):
            if 'from' in time:
                data['from_time'] = str_to_time(time['from'])
            if 'to' in time:
                data['to_time'] = str_to_time(time['to'])

        if training_id := data.get('training'):
            try:
                training = Training.objects.get(id=training_id)
            except Training.DoesNotExist:
                raise serializers.ValidationError({
                    'training': 'Object does not exist.'
                })
            if training.owner != self.context['request'].user:
                raise serializers.ValidationError({
                    'training': 'Permission denied.'
                }, 403)
            data['training'] = training

        if date := data.pop('date', None):
            data['from_date'] = str_to_date(date)

        return data

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'title': instance.title,
            'note': instance.note,
            'place': instance.place,
            'participants': UserSimpleSerializer(instance.participants, many=True).data,
            'time': {
                'from': instance.from_time,
                'to': instance.to_time
            },
            'training': instance.training.id if instance.training else None,
            'single': self.is_single_event(instance),
            'owner': UserSimpleSerializer(instance.owner).data
        }

    def create(self, validated_data):
        validated_data['weekdays'] = validated_data.get('weekdays', [validated_data['from_date'].weekday()])

        if (recurrence := validated_data.pop('recurrence', None)) is None:
            validated_data['to_date'] = validated_data['from_date']
        else:
            validated_data['weekdays'] = recurrence.get('weekdays', validated_data['weekdays'])
            validated_data['interval'] = recurrence.get('interval', 1)

            if last_event_date := recurrence.get('last_event_date'):
                validated_data['to_date'] = str_to_date(last_event_date)
            elif occurrences := recurrence.get('occurrences'):
                validated_data['to_date'] = \
                    calculate_last_occurrence(validated_data['from_date'], validated_data['weekdays'],
                                              occurrences, validated_data['interval'])
            else:
                validated_data['to_date'] = datetime.max.date()

        validated_data['owner'] = self.context['request'].user

        return super().create(validated_data)

    def update(self, instance, validated_data):
        in_data_or_raise_validation_error(validated_data, ['update'])
        in_data_or_raise_validation_error(validated_data['update'], ['recurrence'])
        update_type = validated_data['update']['recurrence']

        if update_type != 'all':
            in_data_or_raise_validation_error(validated_data['update'], ['date'])
            date = str_to_date(validated_data['update']['date'])

            if date > instance.from_date:
                prev_instance = copy(instance)
                prev_instance.pk = None
                prev_instance.to_date = date - timedelta(days=1)
                prev_instance.save()

                if update_type == 'next':
                    validated_data['from_date'] = date

            if update_type == 'no' and date < instance.to_date:
                next_instance = copy(instance)
                next_instance.pk = None
                next_instance.from_date = date + timedelta(days=1)
                next_instance.save()

                in_data_or_raise_validation_error(validated_data, ['from_date'])
                validated_data['to_date'] = validated_data['from_date']

        if self.is_single_event(instance):
            validated_data['weekdays'] = [instance.from_date.weekday()]
            if date := validated_data.get('from_date'):
                validated_data['from_date'] = date
                validated_data['to_date'] = date
        elif recurrence := validated_data.pop('recurrence', None):
            if last_event_date := recurrence.pop('last_event_date', None):
                validated_data['to_date'] = str_to_date(last_event_date)
            elif occurrences := recurrence.pop('occurrences', None):
                validated_data['to_date'] = \
                    calculate_last_occurrence(validated_data.get('from_date', instance.from_date),
                                              recurrence.get('weekdays', instance.weekdays),
                                              occurrences, recurrence.get('interval', instance.interval))

            validated_data |= recurrence
        return super().update(instance, validated_data)
