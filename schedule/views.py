import copy
from datetime import timedelta

from django.db.models import Q
from rest_framework import generics, status
from rest_framework.response import Response

from fituu.permissions import IsOwnerPermission
from schedule.models import Event
from schedule.serializers import EventSerializer
from schedule.utils import str_to_date, send_new_event_notification, send_delete_event_notification, \
    send_delete_all_events_notification, send_edit_event_notification


class EventListCreateView(generics.ListCreateAPIView):
    serializer_class = EventSerializer
    queryset = Event.objects.all()

    def list(self, request, *args, **kwargs):
        from_date = str_to_date(request.query_params.get('from'))
        to_date = str_to_date(request.query_params.get('to'))

        required_participation = request.query_params.get('required_participation', False)

        if required_participation:
            queryset = self.get_queryset().filter(participants__in=[request.user])
        else:
            queryset = self.get_queryset().filter(Q(participants__in=[request.user]) | Q(owner=request.user)).distinct()

        data = []
        while to_date >= from_date:
            queryset2 = queryset.filter(from_date__lte=from_date).filter(to_date__gte=from_date)
            queryset2 = queryset2.filter(weekdays__contains=[from_date.weekday()]).order_by('from_time')

            day_data = []
            for event in queryset2:
                if (from_date - event.from_date) // timedelta(days=7) % event.interval == 0:
                    day_data.append(event)

            data.append({
                'date': from_date,
                'events': self.get_serializer(day_data, many=True).data
            })
            from_date += timedelta(days=1)

        return Response(data)

    def perform_create(self, serializer):
        event = serializer.save()
        send_new_event_notification(event, self.request.user)


class EventRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = EventSerializer
    queryset = Event.objects.all()
    permission_classes = [IsOwnerPermission]

    def perform_update(self, serializer):
        event = serializer.save()
        send_edit_event_notification(event, self.request.user)

    def destroy(self, request, *args, **kwargs):
        recurrence = request.data.get('recurrence')
        if recurrence is None or 'date' not in request.data:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        instance = self.get_object()

        if recurrence == "all":
            send_delete_all_events_notification(instance, self.request.user)
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)

        date = str_to_date(request.data['date'])
        if date > instance.to_date or date < instance.from_date:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        if recurrence == "no":
            next_instance = copy.copy(instance)
            next_instance.pk = None
            next_instance.from_date = date + timedelta(days=1)
            next_instance.save()
        instance.to_date = date - timedelta(days=1)
        instance.save()
        send_delete_event_notification(instance, self.request.user, recurrence, date)
        return Response(status=status.HTTP_204_NO_CONTENT)

