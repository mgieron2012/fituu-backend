from datetime import timedelta, datetime

from rest_framework.exceptions import APIException

from notifications.core import notify


def calculate_last_occurrence(start_date, weekdays, occurrences, interval):
    occurrences -= 1
    weekdays_number = len(weekdays)

    if not weekdays_number:
        return start_date

    weekdays = sorted(weekdays)
    start_date_weekday = start_date.weekday()

    first_occurrence_weekday = start_date_weekday

    while first_occurrence_weekday not in weekdays:
        first_occurrence_weekday += 1
        first_occurrence_weekday %= 7

    first_occurrence_weekday_index = weekdays.index(first_occurrence_weekday)

    start_date += timedelta(days=occurrences // weekdays_number * 7 * interval)
    start_date += timedelta(days=(weekdays[(occurrences % weekdays_number + first_occurrence_weekday_index)
                                           % weekdays_number] - start_date_weekday) % 7)

    return start_date


def str_to_date(date):
    fmts = ('%Y-%m-%d', '%Y-%m-%dT%H:%M:%S')
    for fmt in fmts:
        try:
            return datetime.strptime(date, fmt).date()
        except (ValueError, TypeError):
            pass
    raise IncorrectDateTimeFormatError(date, fmts)


def str_to_time(time):
    fmts = ('%H:%M', '%H:%M:%S', '%Y-%m-%dT%H:%M:%S')
    for fmt in fmts:
        try:
            return datetime.strptime(time, fmt).time()
        except (ValueError, TypeError):
            pass
    raise IncorrectDateTimeFormatError(time, fmts)


class IncorrectDateTimeFormatError(APIException):
    status_code = 400

    def __init__(self, value, formats):
        self.detail = f'{value} does not fit any date-time formats from {formats}.'


def send_new_event_notification(event, owner):
    notify("Zaplanowano wydarzenie",
           f"{owner.first_name} {owner.last_name} zaplanował{'a' if owner.is_female else ''} wydarzenie {event.title}. "
           "Zajrzyj do swojego kalendarza!",
           event.participants.exclude(id=owner.id))


def send_edit_event_notification(event, user):
    notify("Edytowano wydarzenie",
           f"{user.first_name} {user.last_name} wprowadził{'a' if user.is_female else ''} zmiany w wydarzeniu "
           f"{event.title}. Sprawdź zmiany w swoim kalendarzu!",
           event.participants.exclude(id=user.id))


def send_delete_event_notification(event, user, recurrence, date):
    notify("Odwołano wydarzenie",
            f"{user.first_name} {user.last_name} odwołał{'a' if user.is_female else ''} wydarzenie {event.title}"
            f" odbywające się w dniu {date.strftime('%d.%m.%Y')}{' i później' if recurrence == 'next' else ''}.",
            event.participants.exclude(id=user.id))


def send_delete_all_events_notification(event, user):
    notify("Odwołano wydarzenie",
           f"{user.first_name} {user.last_name} odwołał{'a' if user.is_female else ''} wydarzenie "
           f"{event.title}.",
           event.participants.exclude(id=user.id))