import math

from rest_framework import serializers

from fituu.serializers import UserSimpleSerializer
from payments import settings
from payments.core import create_payment_intend, update_payment_intend
from payments.models import StripeAccount, Purchase
from payments.exceptions import AccountDoesNotAcceptPayoutsError, CreatingBillNotForClientError,\
    UnexpectedStripeError, UserNotParticipateInTransactionError


class StripeAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = StripeAccount
        fields = ['id', 'details_submitted', 'payouts_enabled']


class PurchaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Purchase
        fields = ['id', 'seller', 'buyer', 'created_at', 'deadline', 'title', 'description', 'amount',
                  'payment_completed', 'transfer_fee', 'is_created_by_seller']
        read_only_fields = ['id', 'created_at', 'payment_completed', 'transfer_fee', 'is_created_by_seller']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if self.context['request'].user == instance.buyer:
            representation |= {'client_secret': instance.payment_client_secret}

        return representation | {
            'buyer': UserSimpleSerializer(instance.buyer).data if instance.buyer else None,
            'seller': UserSimpleSerializer(instance.seller).data if instance.seller else None
        }

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['seller'] = validated_data.get('seller', user)
        validated_data['buyer'] = validated_data.get('buyer', user)

        if (validated_data['seller'] == user) == (validated_data['buyer'] == user):
            raise UserNotParticipateInTransactionError()
        validated_data['is_created_by_seller'] = (validated_data['seller'] == user)

        if validated_data['is_created_by_seller'] and validated_data['buyer'] not in [client.client for client in
                                                                                      user.clients.all()]:
            raise CreatingBillNotForClientError()

        validated_data['transfer_fee'] = math.floor(validated_data['amount'] * settings.TRANSFER_FEE_IN_PERCENT / 100)
        if not validated_data['seller'].stripe_account.exists() or \
                not validated_data['seller'].stripe_account.first().payouts_enabled:
            raise AccountDoesNotAcceptPayoutsError()
        payment_id, client_secret = \
            create_payment_intend(validated_data['amount'],
                                  validated_data['transfer_fee'],
                                  validated_data['title'],
                                  validated_data['seller'].stripe_account.first().account_id,
                                  validated_data['buyer'].email)
        if not (payment_id and client_secret):
            raise UnexpectedStripeError()

        return Purchase.objects.create(**validated_data, client_secret=client_secret, payment_id=payment_id)

    def update(self, instance, validated_data):
        amount = validated_data.pop('amount', None)
        validated_data.pop('seller', None)
        validated_data.pop('buyer', None)

        instance = super().update(instance, validated_data)

        if amount and amount != instance.amount:
            instance.amount = amount
            instance.transfer_fee = math.floor(amount * settings.TRANSFER_FEE_IN_PERCENT / 100)
            client_secret = update_payment_intend(instance.payment_id, amount, instance.transfer_fee, instance.title)
            if not client_secret:
                raise UnexpectedStripeError()
            instance.client_secret = client_secret
            instance.save()

        return instance
