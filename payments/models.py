from django.db import models


class Purchase(models.Model):
    id = models.AutoField(primary_key=True)
    seller = models.ForeignKey('fituu.User', on_delete=models.SET_NULL, null=True, related_name='purchased')
    buyer = models.ForeignKey('fituu.User', on_delete=models.SET_NULL, null=True, related_name='sold')
    created_at = models.DateTimeField(auto_now=True)
    deadline = models.DateTimeField(null=True)
    title = models.CharField(max_length=500)
    description = models.CharField(blank=True, null=True, max_length=1000)
    currency = models.CharField(max_length=10, default='pln')
    amount = models.PositiveIntegerField()
    transfer_fee = models.PositiveIntegerField()
    payment_id = models.CharField(max_length=100)
    payment_completed = models.BooleanField(default=False)
    payment_client_secret = models.CharField(max_length=100)
    is_created_by_seller = models.BooleanField(default=True)

    class Meta:
        ordering = ['-created_at']


class StripeAccount(models.Model):
    id = models.AutoField(primary_key=True)
    owner = models.ForeignKey('fituu.User', on_delete=models.CASCADE, related_name='stripe_account')
    account_id = models.CharField(max_length=30)
    details_submitted = models.BooleanField(default=False)
    payouts_enabled = models.BooleanField(default=False)
