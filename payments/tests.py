from fituu.models import User
from fituu.tests import MyAPITestCase
from notifications.models import NotificationStatus

from payments.core import account_get_or_create, get_url_to_account_page
from payments.models import Purchase
from payments.utils import send_new_payment_notification, send_payment_updated_notification
from payments.views import PurchaseListCreateView, GetAccountDetailsView, PurchaseRetrieveUpdateDestroyView


class TestCore(MyAPITestCase):
    def setUp(self):
        super().setUp()
        self.purchase = Purchase.objects.create(seller=self.user, buyer=User.objects.first(), title="p1",
                                                amount=1000, transfer_fee=50)

    def test_create_account(self):
        account = account_get_or_create(self.user)
        self.assertIsNotNone(account)

    def test_get_url(self):
        account = account_get_or_create(self.user)
        url = get_url_to_account_page(account.account_id)
        self.assertIsNotNone(url)

    def test_create_purchase_455(self):
        data = {
            'buyer': User.objects.first().id,
            'title': "purchase 1",
            'description': 'few trainings',
            'amount': 25000
        }
        response = self.auth_request('url', PurchaseListCreateView, "POST", data)
        self.assertEqual(response.status_code, 455)

    def test_create_purchase_454(self):
        self.user.clients.all().delete()
        data = {
            'seller': self.user.id,
            'buyer': User.objects.first().id,
            'title': "purchase 1",
            'description': 'few trainings',
            'amount': 25000
        }
        response = self.auth_request('url', PurchaseListCreateView, "POST", data)
        self.assertEqual(response.status_code, 454)

    def test_create_purchase_400(self):
        data = {
            'buyer': User.objects.first().id,
            'title': "purchase 1",
            'description': 'few trainings',
            'amount': -1500
        }
        response = self.auth_request('url', PurchaseListCreateView, "POST", data)
        self.assertEqual(response.status_code, 400)

    def test_create_purchase_457(self):
        data = {
            'buyer': self.user.id,
            'title': "purchase 1",
            'description': 'few trainings',
            'amount': 25000
        }
        response = self.auth_request('url', PurchaseListCreateView, "POST", data)
        self.assertEqual(response.status_code, 457)

    def test_update_purchase(self):
        data = {
            'title': "new title"
        }
        response = self.auth_request('url', PurchaseRetrieveUpdateDestroyView, "PATCH", data=data, pk=self.purchase.id)
        self.assertEqual(response.data['title'], "new title")
        self.assertEqual(response.data['id'], self.purchase.id)

    def test_update_purchase_458(self):
        data = {
            'title': "new title"
        }
        self.purchase.payment_completed = True
        self.purchase.save()
        response = self.auth_request('url', PurchaseRetrieveUpdateDestroyView, "PATCH", data=data, pk=self.purchase.id)
        self.assertEqual(response.status_code, 458)

    def test_update_purchase_403(self):
        data = {
            'title': "new title"
        }
        self.purchase.is_created_by_seller = False
        self.purchase.save()
        response = self.auth_request('url', PurchaseRetrieveUpdateDestroyView, "PATCH", data=data, pk=self.purchase.id)
        self.assertEqual(response.status_code, 403)

    def test_delete_purchase(self):
        response = self.auth_request('url', PurchaseRetrieveUpdateDestroyView, "DELETE", pk=self.purchase.id)
        self.assertEqual(response.status_code, 551)

    def test_delete_purchase_458(self):
        self.purchase.payment_completed = True
        self.purchase.save()
        response = self.auth_request('url', PurchaseRetrieveUpdateDestroyView, "DELETE", pk=self.purchase.id)
        self.assertEqual(response.status_code, 458)

    def test_delete_purchase_458(self):
        self.purchase.payment_completed = True
        self.purchase.save()
        response = self.auth_request('url', PurchaseRetrieveUpdateDestroyView, "DELETE", pk=self.purchase.id)
        self.assertEqual(response.status_code, 458)

    def test_get_purchase(self):
        response = self.auth_request('url', PurchaseRetrieveUpdateDestroyView, "GET", pk=self.purchase.id)
        self.assertNotIn('client_secret', response.data)
        self.assertEqual(response.data['title'], self.purchase.title)
        self.assertEqual(response.data['is_created_by_seller'], self.purchase.is_created_by_seller)
        self.assertEqual(response.data['seller']['id'], self.purchase.seller.id)

    def test_get_purchase_as_buyer(self):
        self.purchase.buyer = self.user
        self.purchase.save()
        response = self.auth_request('url', PurchaseRetrieveUpdateDestroyView, "GET", pk=self.purchase.id)
        self.assertIn('client_secret', response.data)

    def test_get_purchase_403(self):
        self.purchase.seller = User.objects.first()
        self.purchase.save()
        response = self.auth_request('url', PurchaseRetrieveUpdateDestroyView, "GET", pk=self.purchase.id)
        self.assertEqual(response.status_code, 403)

    def test_list_purchase(self):
        data = {
            'role': 'buyer'
        }
        response = self.auth_request('url', PurchaseListCreateView, "GET", data=data)
        self.assertEqual(len(response.data), Purchase.objects.filter(buyer=self.user).count())

    def test_list_purchase_as_seller(self):
        data = {
            'role': 'seller'
        }
        response = self.auth_request('url', PurchaseListCreateView, "GET", data=data)
        self.assertEqual(len(response.data), Purchase.objects.filter(seller=self.user).count())

    def test_get_account_details(self):
        account = account_get_or_create(self.user)
        response = self.auth_request('url', GetAccountDetailsView, "GET")
        self.assertEqual(response.data['payouts_enabled'], False)
        self.assertEqual(response.data['details_submitted'], False)
        account.payouts_enabled = True
        account.save()
        response = self.auth_request('url', GetAccountDetailsView, "GET")
        self.assertEqual(response.data['payouts_enabled'], True)

    def test_send_new_notification(self):
        send_new_payment_notification(self.purchase)
        notification = NotificationStatus.objects.get(owner=self.purchase.buyer).notification
        self.assertEqual(notification.title, 'Nowa płatność')
        self.assertEqual(notification.content, 'Marcin  wystawił rachunek p1.')

    def test_send_update_notification(self):
        send_payment_updated_notification(self.purchase)
        notification = NotificationStatus.objects.get(owner=self.purchase.buyer).notification
        self.assertEqual(notification.title, 'Zmiana w płatności')
        self.assertEqual(notification.content, 'Marcin  zmienił szczegóły transakcji p1.')
