import logging

from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from payments.core import get_url_to_account_page, decode_webhook_body, account_get_or_create, delete_payment_intend
from payments.exceptions import UnexpectedStripeError, CompletedPurchaseError
from payments.models import StripeAccount, Purchase
from payments.permissions import PurchasePermission
from payments.serializers import StripeAccountSerializer, PurchaseSerializer
from payments.settings import ENDPOINT_SECRETS
from payments.utils import send_new_payment_notification, send_payment_updated_notification


class PurchaseListCreateView(generics.ListCreateAPIView):
    serializer_class = PurchaseSerializer
    queryset = Purchase.objects.all()

    def filter_queryset(self, queryset):
        role = self.request.query_params.get('role', 'buyer')
        if role == 'buyer':
            return queryset.filter(buyer=self.request.user)
        else:
            return queryset.filter(seller=self.request.user)

    def perform_create(self, serializer):
        serializer.save()
        send_new_payment_notification(serializer.instance)


class PurchaseRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PurchaseSerializer
    permission_classes = [IsAuthenticated & PurchasePermission]
    queryset = Purchase.objects.all()

    def perform_update(self, serializer):
        if serializer.instance.payment_completed:
            raise CompletedPurchaseError()
        serializer.save()
        send_payment_updated_notification(serializer.instance)

    def perform_destroy(self, instance):
        if instance.payment_completed:
            raise CompletedPurchaseError()
        if delete_payment_intend(instance.payment_id):
            instance.delete()
        else:
            raise UnexpectedStripeError()


class GetAccountDetailsView(APIView):
    def get(self, request):
        try:
            return Response(status=200, data=StripeAccountSerializer(request.user.stripe_account.first()).data)
        except StripeAccount.DoesNotExist:
            return Response(status=404)


@api_view(['POST'])
@csrf_exempt
@permission_classes([AllowAny])
def hook_on_account_updated_view(request):
    try:
        event = decode_webhook_body(request.body, request.META['HTTP_STRIPE_SIGNATURE'],
                                    ENDPOINT_SECRETS['ACCOUNT'])
    except ValueError as e:
        logging.error(e)
        return Response(status=400)

    account = event.data.object

    if account_id := account.get('id'):
        details_submitted = account.get('details_submitted', False)
        payouts_enabled = account.get('payouts_enabled', False)
        try:
            account = StripeAccount.objects.get(account_id=account_id)
            account.details_submitted = details_submitted
            account.payouts_enabled = payouts_enabled
            account.save()
        except StripeAccount.DoesNotExist:
            logging.error('Webhook returned incorrect account.id!')
    else:
        logging.error('Data returned by webhook does not have account.id!')

    return Response(status=200)


@api_view(['POST'])
@csrf_exempt
@permission_classes([AllowAny])
def hook_on_payment_intent_succeeded(request):
    try:
        event = decode_webhook_body(request.body, request.META['HTTP_STRIPE_SIGNATURE'],
                                    ENDPOINT_SECRETS['PAYMENT'])
    except ValueError as e:
        logging.error(e)
        return Response(status=400)

    payment = event.data.object
    try:
        purchase = Purchase.objects.get(payment_id=payment.get('id'),
                                        payment_client_secret=payment.get('client_secret'))
        purchase.payment_completed = True
        purchase.save()
    except Purchase.DoesNotExist:
        logging.error('Completed payment for purchase that does not exist.')
    return Response(status=200)


@api_view(['GET'])
@csrf_exempt
def redirect_to_stripe_account_page_view(request):
    account = account_get_or_create(request.user)
    url = get_url_to_account_page(account.account_id)
    if url:
        return redirect(url)
    else:
        raise UnexpectedStripeError()
