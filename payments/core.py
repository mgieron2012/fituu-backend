import logging

import stripe

from payments import settings
from payments.exceptions import UnexpectedStripeError
from payments.models import StripeAccount

stripe.api_key = settings.STRIPE_KEY


def account_get_or_create(user):
    try:
        account, _ = StripeAccount.objects.get_or_create(owner=user, defaults={
            'account_id': stripe.Account.create(type="express",
                                                country="PL",
                                                email=user.email,
                                                capabilities={
                                                    "card_payments": {"requested": True},
                                                    "transfers": {"requested": True},
                                                    "p24_payments": {"requested": True}  # przelewy24
                                                }).get('id')
        })
        return account

    except Exception as e:
        logging.error(e)
        raise UnexpectedStripeError()


def get_url_to_account_page(account_id):
    try:
        return stripe.AccountLink.create(
            account=account_id,
            refresh_url=settings.REFRESH_URL,
            return_url=settings.RETURN_URL,
            type='account_onboarding',
        ).get('url')
    except Exception as e:
        logging.error(e)
        raise UnexpectedStripeError()


def decode_webhook_body(body, signature, endpoint_secret):
    try:
        return stripe.Webhook.construct_event(
            body, signature, endpoint_secret
        )
    except Exception as e:
        logging.error(e)
        raise UnexpectedStripeError()


def create_payment_intend(amount, fee_amount, description, seller_account_id, buyer_email):
    try:
        payment = stripe.PaymentIntent.create(
            payment_method_types=['card', 'p24'],
            amount=amount,
            currency='pln',
            application_fee_amount=fee_amount,
            transfer_data={
                'destination': seller_account_id,
            },
            receipt_email=buyer_email,
            description=description
        )
    except Exception as e:
        logging.error(e)
        raise UnexpectedStripeError()

    return payment.get('id'), payment.get('client_secret')


def update_payment_intend(payment_id, amount, fee_amount, description):
    try:
        return stripe.PaymentIntent.modify(
            payment_id,
            amount=amount,
            application_fee_amount=fee_amount,
            description=description
        ).get('client_secret')
    except Exception as e:
        logging.error(e)
        raise UnexpectedStripeError()


def delete_payment_intend(payment_id):
    try:
        return stripe.PaymentIntent.cancel(payment_id).get('id')
    except Exception as e:
        logging.error(e)
        raise UnexpectedStripeError()
