from rest_framework.permissions import BasePermission


class PurchasePermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method == "GET":
            return request.user in [obj.buyer, obj.seller]
        else:
            if obj.is_created_by_seller:
                return request.user == obj.seller
            else:
                return request.user == obj.buyer
