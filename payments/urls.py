from django.urls import path

from payments.views import GetAccountDetailsView, hook_on_account_updated_view, redirect_to_stripe_account_page_view,\
    PurchaseListCreateView, PurchaseRetrieveUpdateDestroyView, hook_on_payment_intent_succeeded

urlpatterns = [
    path('purchase/', PurchaseListCreateView.as_view()),
    path('purchase/<int:pk>/', PurchaseRetrieveUpdateDestroyView.as_view()),
    path('account/', GetAccountDetailsView.as_view()),
    path('account/update/', redirect_to_stripe_account_page_view),
    path('hooks/account/', hook_on_account_updated_view),
    path('hooks/payment/', hook_on_payment_intent_succeeded)
]
