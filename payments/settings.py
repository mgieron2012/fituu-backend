import os

STRIPE_KEY = os.getenv("STRIPE_KEY", "sk_test_51JS5GQFFpoBN1r1zsqlba3sl8PqyXB74v7IMNhXU7NHO24MCP1V0bM2nJwkLfcAj2Hy5MAhWbFkP5kYLjAhXE5fr00WjWyDAsX")

REFRESH_URL = 'https://digitalocean.pl/payments/account/update/'

RETURN_URL = 'https://fituu.pl'

ENDPOINT_SECRETS = {
    'ACCOUNT': os.getenv("STRIPE_ENDPOINT_SECRET_ACCOUNT", 'whsec_vUB8g4b5PCgxcDLvKZI6ME8faY3yLaXI'),
    'PAYMENT': os.getenv("STRIPE_ENDPOINT_SECRET_PAYMENT", 'whsec_vUB8g4b5PCgxcDLvKZI6ME8faY3yLaXI')
}

TRANSFER_FEE_IN_PERCENT = 5
