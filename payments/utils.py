from notifications.core import notify


def send_new_payment_notification(purchase):
    if purchase.is_created_by_seller:
        notify('Nowa płatność',
               f'{purchase.seller.first_name} {purchase.seller.last_name} '
               f'wystawił{"a" if purchase.seller.is_female else "" } rachunek {purchase.title}.',
               [purchase.buyer])


def send_payment_updated_notification(purchase):
    if purchase.is_created_by_seller:
        notify('Zmiana w płatności',
               f'{purchase.seller.first_name} {purchase.seller.last_name} '
               f'zmienił{"a" if purchase.seller.is_female else "" } szczegóły transakcji {purchase.title}.',
               [purchase.buyer])
