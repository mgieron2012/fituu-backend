from rest_framework.exceptions import APIException


class CreatingBillNotForClientError(APIException):
    status_code = 454
    default_detail = "User can create bill only for its clients"


class AccountDoesNotAcceptPayoutsError(APIException):
    status_code = 455
    default_detail = "Seller does not have properly configured account to accept payouts"


class UnexpectedStripeError(APIException):
    status_code = 551
    default_detail = "Unexpected stripe error occurred"


class UserNotParticipateInTransactionError(APIException):
    status_code = 457
    default_detail = "User is not one of the parties to the transaction"


class CompletedPurchaseError(APIException):
    status_code = 458
    default_detail = "Completed purchase can neither be updated nor deleted."
