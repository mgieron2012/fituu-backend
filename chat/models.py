from django.db import models


class Message(models.Model):
    id = models.AutoField(primary_key=True)
    author = models.ForeignKey('fituu.User', on_delete=models.CASCADE)
    conversation = models.ForeignKey('Conversation', on_delete=models.CASCADE, related_name="messages")
    timestamp = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    attachment = models.ForeignKey('storage.StorageFile', on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ['-timestamp']


class Conversation(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(default="")
    participants = models.ManyToManyField('fituu.User')
    last_message = models.ForeignKey(Message, on_delete=models.SET_NULL, null=True, related_name="lasts")

    class Meta:
        ordering = ['-last_message__timestamp']
