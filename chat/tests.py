import datetime

from rest_framework import status

from chat.models import Conversation, Message
from chat.views import ListCreateConversationView, RetrieveUpdateDestroyConversationView, ListCreateMessageView, \
    RetrieveUpdateDestroyMessageView
from fituu.models import User
from fituu.tests import MyAPITestCase
from storage.models import StorageFile


class ChatTest(MyAPITestCase):
    def test_create_conversation(self):
        data_to_send = {
            'participants': [User.objects.first().id, self.user.id],
            'description': 'Pn 12:00'
        }
        response = self.auth_request("/chat/conversation/", ListCreateConversationView, "POST", data_to_send)
        self.assertIn('id', response.data)
        self.assertEqual(response.data['description'], 'Pn 12:00')
        self.assertEqual(len(response.data['participants']), 2)

    def test_create_conversation_no_self_id_in_participants(self):
        data_to_send = {
            'participants': [User.objects.first().id]
        }
        response = self.auth_request("/chat/conversation/", ListCreateConversationView, "POST", data_to_send)
        self.assertEqual(len(response.data['participants']), 2)

    def test_update_conversation(self):
        c = Conversation.objects.create(description="Con 1")
        c.participants.set([User.objects.first().id, self.user.id])
        data_to_send = {
            'participants': [User.objects.first().id],
            'description': "new desc"
        }
        response = self.auth_request("/chat/conversation/", RetrieveUpdateDestroyConversationView,
                                     "PATCH", data_to_send, pk=c.id)
        data = response.data
        self.assertEqual(len(data['participants']), 1)
        self.assertIsNone(data['last_message'])
        c.refresh_from_db()
        self.assertIsNotNone(c.participants.get(pk=User.objects.first().id))
        self.assertEqual(c.description, data_to_send['description'])

    def test_update_conversation_no_permission(self):
        User.objects.create()
        c = Conversation.objects.create(description="Con 1")
        c.participants.set([User.objects.first().id, User.objects.all()[3].id])
        data_to_send = {
            'participants': [self.user.id],
            'description': "new desc"
        }
        response = self.auth_request("/chat/conversation/",
                                     RetrieveUpdateDestroyConversationView, "PATCH", data_to_send, pk=c.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_details(self):
        c = Conversation.objects.create(description="Con 1")
        c.participants.set([User.objects.first().id, self.user.id])
        response = self.auth_request(f"/chat/conversation/", RetrieveUpdateDestroyConversationView, "GET", pk=c.id)
        data = response.data
        self.assertEqual(data['description'], c.description)
        self.assertIsNone(data['last_message'])
        self.assertEqual(data['participants'][1]['email'], self.user.email)

    def test_get_details_no_permission(self):
        c = Conversation.objects.create(description="Con 1")
        c.participants.set([User.objects.first().id])
        response = self.auth_request(f"/chat/conversation/", RetrieveUpdateDestroyConversationView, "GET", pk=c.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_conversation(self):
        for i in range(10):
            c = Conversation.objects.create(description=f"Con {i}")
            if i % 2:
                c.participants.set([self.user.id])
                m = Message.objects.create(conversation=c, content="A", author_id=self.user.id)
                c.last_message = m
                c.save()
        response = self.auth_request("/chat/conversation/list/", ListCreateConversationView, "GET")
        data = response.data
        self.assertEqual(len(data), 5)
        self.assertGreater(data[0]['last_message']['timestamp'], data[4]['last_message']['timestamp'])

    def test_delete_conversation(self):
        c = Conversation.objects.create(description="Con 1")
        c.participants.set([User.objects.first().id, self.user.id])
        response = self.auth_request("/chat/conversation/", RetrieveUpdateDestroyConversationView, "DELETE", pk=c.id)
        self.assertEqual(response.status_code, 204)
        with self.assertRaises(Conversation.DoesNotExist):
            c.refresh_from_db()

    def test_delete_conversation_no_permission(self):
        c = Conversation.objects.create(description="Con 12")
        c.participants.set([User.objects.first().id])
        response = self.auth_request("/chat/conversation/", RetrieveUpdateDestroyConversationView, "DELETE", pk=c.id)
        self.assertEqual(response.status_code, 403)
        c.refresh_from_db()

    def test_message_create(self):
        c = Conversation.objects.create(description="Con 1")
        c.participants.add(self.user)
        data_to_send = {
            'conversation': c.id,
            'content': 'message',
            'attachment': StorageFile.objects.first().id
        }
        response = self.auth_request('/chat/message/create/', ListCreateMessageView, "POST", data_to_send)
        data = response.data
        self.assertIn('id', data)
        c.refresh_from_db()
        self.assertEqual(c.last_message.id, data['id'])

    def test_message_list(self):
        c = Conversation.objects.create(description="Con 1")
        c.participants.add(self.user)

        for i in range(10):
            Message.objects.create(content=f"Mes {i}", author=self.user, conversation=c)

        data_to_send = {
            'conversation': c.id
        }
        response = self.auth_request("/chat/message/list/", ListCreateMessageView, 'GET', data=data_to_send)
        self.assertEqual(len(response.data), 10)

    def test_message_list_empty(self):
        c = Conversation.objects.create(description="Con 1")
        c.participants.add(self.user)

        for i in range(10):
            Message.objects.create(content=f"Mes {i}", author=self.user, conversation=c)

        data_to_send = {
            'conversation': c.id,
            'timestamp': datetime.datetime.max
        }
        response = self.auth_request("/chat/message/list/", ListCreateMessageView, 'GET', data=data_to_send)
        self.assertEqual(len(response.data), 0)

    def test_message_list_few(self):
        c = Conversation.objects.create(description="Con 1")
        c.participants.add(self.user)

        for i in range(10):
            Message.objects.create(content=f"Mes {i}", author=self.user, conversation=c)

        data_to_send = {
            'conversation': c.id,
            'timestamp': Message.objects.all()[4].timestamp
        }
        response = self.auth_request("/chat/message/list/", ListCreateMessageView, 'GET', data=data_to_send)
        self.assertEqual(len(response.data), 4)

    def test_message_list_no_perm(self):
        c = Conversation.objects.create(description="Con 1")

        for i in range(10):
            Message.objects.create(content=f"Mes {i}", author=self.user, conversation=c)

        data_to_send = {
            'conversation': c.id,
            'last_message': 6
        }

        response = self.auth_request("/chat/message/list/", ListCreateMessageView, 'GET', data=data_to_send)
        self.assertEqual(response.status_code, 403)

    def test_message_update(self):
        c = Conversation.objects.create(description="Con 1")
        c2 = Conversation.objects.create(description="Con 2")
        c.participants.add(self.user)
        m = Message.objects.create(conversation=c, content='hi!', author=self.user)

        data_to_send = {
            'conversation': c2.id,
            'content': 'message',
            'attachment': StorageFile.objects.first().id
        }
        response = self.auth_request('/chat/message/', RetrieveUpdateDestroyMessageView, "PATCH", data_to_send, pk=m.id)
        data = response.data
        self.assertEqual(data['content'], data_to_send['content'])
        self.assertIn('id', data)
        m.refresh_from_db()
        self.assertEqual(m.attachment, StorageFile.objects.first())
        self.assertEqual(m.conversation, c)

    def test_message_update_no_perm_attachment(self):
        c = Conversation.objects.create(description="Con 1")
        c.participants.add(self.user)
        m = Message.objects.create(conversation=c, content='hi!', author=self.user)
        f = StorageFile.objects.first()
        f.owner = User.objects.first()
        f.save()

        data_to_send = {
            'content': 'message',
            'attachment': f.id
        }

        response = self.auth_request('/chat/message/', RetrieveUpdateDestroyMessageView, "PATCH", data_to_send, pk=m.id)
        self.assertEqual(response.status_code, 400)

    def test_message_retrieve(self):
        c = Conversation.objects.create(description="Con 1")
        m = Message.objects.create(conversation=c, content='hi!', author=self.user,
                                   attachment=StorageFile.objects.first())

        response = self.auth_request('/chat/message/', RetrieveUpdateDestroyMessageView, "GET", pk=m.id)
        data = response.data
        self.assertEqual(data['id'], m.id)
        self.assertEqual(data['content'], m.content)
        self.assertEqual(data['author'], m.author_id)
        self.assertIn('url', data['attachment']['file'])

    def test_message_delete(self):
        c = Conversation.objects.create(description="Con 1")
        m = Message.objects.create(conversation=c, content='hi!', author=self.user)

        response = self.auth_request('/chat/message/', RetrieveUpdateDestroyMessageView, "DELETE", pk=m.id)
        self.assertEqual(response.status_code, 204)
        with self.assertRaises(Message.DoesNotExist):
            m.refresh_from_db()

    def test_message_delete_no_perm(self):
        c = Conversation.objects.create(description="Con 1")
        m = Message.objects.create(conversation=c, content='hi!', author=User.objects.first())

        response = self.auth_request('/chat/message/', RetrieveUpdateDestroyMessageView, "DELETE", pk=m.id)
        self.assertEqual(response.status_code, 403)
        m.refresh_from_db()
