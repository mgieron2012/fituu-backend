from rest_framework import serializers
from chat.models import Message, Conversation
from fituu.serializers import UserSimpleSerializer
from storage.serializers import StorageFileSerializer


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        exclude = ['author', 'timestamp']

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'content': instance.content,
            'author': instance.author.id,
            'timestamp': instance.timestamp,
            'attachment': StorageFileSerializer(instance.attachment).data if instance.attachment else None
        }

    def validate_attachment(self, value):
        if value is None or value.owner == self.context['request'].user:
            return value
        raise serializers.ValidationError({
            'attachment': 'Permission denied.'
        }, code=403)

    def update(self, instance, validated_data):
        validated_data.pop('conversation', None)
        return super().update(instance, validated_data)


class ConversationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conversation
        exclude = ['last_message']

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'description': instance.description,
            'participants': UserSimpleSerializer(instance.participants, many=True).data,
            'last_message': MessageSerializer(instance.last_message).data if instance.last_message else None
        }
