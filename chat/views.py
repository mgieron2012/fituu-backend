from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from chat.models import Conversation, Message
from chat.serializers import ConversationSerializer, MessageSerializer
from chat.permissions import IsInParticipantsPermission, IsAuthorPermission


class ListCreateConversationView(generics.ListCreateAPIView):
    serializer_class = ConversationSerializer
    queryset = Conversation.objects.all()

    def filter_queryset(self, queryset):
        return queryset.filter(participants__in=[self.request.user])

    def perform_create(self, serializer):
        if self.request.user not in serializer.validated_data['participants']:
            serializer.validated_data['participants'].append(self.request.user)
        serializer.save()


class RetrieveUpdateDestroyConversationView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ConversationSerializer
    permission_classes = [IsInParticipantsPermission]
    queryset = Conversation.objects.all()


class ListCreateMessageView(generics.ListCreateAPIView):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()

    def get_permissions(self):
        if self.request.method == 'GET':
            return [IsInParticipantsPermission()]
        return [IsAuthenticated()]

    def perform_create(self, serializer):
        instance = serializer.save(author=self.request.user)
        instance.conversation.last_message = instance
        instance.conversation.save()
        return instance

    def filter_queryset(self, queryset):
        if timestamp := self.request.query_params.get('timestamp'):
            queryset = queryset.filter(timestamp__gt=timestamp)
        conversation = self.request.query_params.get('conversation', 0)
        return queryset.filter(conversation__id=conversation)


class RetrieveUpdateDestroyMessageView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
    permission_classes = [IsAuthorPermission]
