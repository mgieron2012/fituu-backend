from rest_framework import permissions

from chat.models import Conversation


class IsInParticipantsPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user in obj.participants.all()

    def has_permission(self, request, view):
        try:
            conversation = request.query_params.get('conversation', request.data.get('conversation'))
            if conversation is None:
                return True
            return request.user in Conversation.objects.get(id=conversation).participants.all()
        except (KeyError, Conversation.DoesNotExist):
            return False


class IsAuthorPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.author == request.user
