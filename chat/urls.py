from django.urls import path

from chat.views import ListCreateMessageView, RetrieveUpdateDestroyMessageView, ListCreateConversationView,\
    RetrieveUpdateDestroyConversationView

urlpatterns = [
    path('conversation/', ListCreateConversationView.as_view()),
    path('conversation/<int:pk>/', RetrieveUpdateDestroyConversationView.as_view()),
    path('message/', ListCreateMessageView.as_view()),
    path('message/<int:pk>/', RetrieveUpdateDestroyMessageView.as_view()),
]
